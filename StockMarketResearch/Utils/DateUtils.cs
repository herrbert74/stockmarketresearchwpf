﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Utils
{
    class DateUtil
    {
        public static DateTime ParseDate(string date)
        {
            return DateTime.ParseExact(date, "dd/MM/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);
        }

        public static DateTime ParseShortYearDate(string date)
        {
            return DateTime.ParseExact(date, "MM/dd/yy",
                                       System.Globalization.CultureInfo.InvariantCulture);
        }

        public static DateTime ParseMySqlDate(string date)
        {
            return DateTime.ParseExact(date, "yyyy-MM-dd",
                                       System.Globalization.CultureInfo.InvariantCulture);
        }

        public static long ConvertToTimestamp(DateTime value)
        {
            //create Timespan by subtracting the value provided from
            //the Unix Epoch
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());

            //return the total seconds (which is a UNIX timestamp)
            return (long)span.TotalSeconds;
        }

        public static string ConvertToMySqlDate(DateTime value)
        {
            return value.ToString("yyyy-MM-dd");
        }

        public static string FormatUnixTimeStamp(long timeStamp)
        {
            var Time = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            Time = Time.AddSeconds(timeStamp);
            return Time.ToShortDateString() + " " + Time.ToShortTimeString();
        }
    }
}
