﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.strategies.rules
{
    public class LargerThanRule : IBaseRule<RuleParameters>
    {
        public double first, second;

        public LargerThanRule(double first, double second)
        {
            this.first = first;
            this.second = second;
        }
        
        bool IBaseRule<RuleParameters>.Validate()
        {
            return first > second;
        }
    }
}
