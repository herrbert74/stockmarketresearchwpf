﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.strategies.rules
{
    public class RangeRule : IBaseRule<RuleParameters>
    {
        public dynamic first, minimum, maximum;
        public RangeRule(dynamic first, dynamic minimum, dynamic maximum)
        {
            this.first = first;
            this.minimum = minimum;
            this.maximum = maximum;
        }

        bool IBaseRule<RuleParameters>.Validate()
        { 
            return first > minimum && first < maximum;
        }
    }
}
