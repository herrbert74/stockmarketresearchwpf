﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.strategies.rules
{
    class RuleParameters
    {
        public int First { get; set; }
        public int Second { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
    }
}
