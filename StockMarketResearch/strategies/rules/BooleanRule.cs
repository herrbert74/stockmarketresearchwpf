﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.strategies.rules
{
    public class BooleanRule : IBaseRule<RuleParameters>
    {
        public bool first;

        public BooleanRule(bool first)
        {
            this.first = first;
        }

        bool IBaseRule<RuleParameters>.Validate()
        {
            return first;
        }
    }
}
