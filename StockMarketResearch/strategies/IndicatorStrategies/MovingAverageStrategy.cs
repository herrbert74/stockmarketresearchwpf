﻿using System;
using StockMarketResearch.strategies.rules;
using StockMarketResearch.Data.Storage;

namespace StockMarketResearch.Strategies.IndicatorStrategies
{
    class MovingAverageStrategy : BaseIndicatorStrategy
    {
        public override bool canOpenTrade(StockDao stockDao, IndicatorDao indicatorDao, int item, StockDao indexStockDao)
        {
            rulesX.Clear();
            rulesX.Add(new BooleanRule(isIndexRising(item, stockDao.series.Count, indexStockDao)));
            /*rulesX.Add(new LargerThanRule(stockDao.series[item].close, indicatorDao.ma20[item]));
            rulesX.Add(new LargerThanRule(indicatorDao.ma20[item], indicatorDao.ma50[item]));
            rulesX.Add(new LargerThanRule(indicatorDao.ma50[item], indicatorDao.ma200[item]));*/
            //rulesX.Add(new RangeRule(indicatorDao.MoneyFlowIndex[item], 85, 95));
            rulesX.Add(new LargerThanRule(stockDao.series[item].close, indicatorDao.ma200[item]));
            rulesX.Add(new LargerThanRule(indicatorDao.ma50[item], stockDao.series[item].close));
            //rulesX.Add(new LargerThanRule(indicatorDao.ma200[item], stockDao.series[item].close));
            rulesX.Add(new RangeRule(indicatorDao.MoneyFlowIndex[item], 0, 10));

            rulesX.Add(new LargerThanRule(item, 10));
            if (item > 10)
            {
                rulesX.Add(new LargerThanRule(indicatorDao.OnVolumeBalance[item], indicatorDao.OnVolumeBalance[item - 8]));
            }
            return validateAllRules(rulesX);
        }

        private static bool isIndexRising(int item, int count, StockDao indexStockDao)
        {
            if(count - item + 2 > indexStockDao.series.Count)
            {
                return false;
            }
            else
            {
                int indexCount = indexStockDao.series.Count;
                return indexStockDao.series[indexCount - (count - item)].close > indexStockDao.series[indexCount - (count - item) - 2].close;
            }
            
        }

        public override int closeTrade(StockDao stockDao, IndicatorDao indicatorDao, int buyItem)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            origin = origin.AddSeconds(stockDao.series[buyItem].Timestamp);
            Boolean sold = false;
            int j = buyItem;
            while (!sold)
            {
                j++;
                DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                jTime = jTime.AddSeconds(stockDao.series[j + 1].Timestamp);
                //Profit goal reached
                if (stockDao.series[j].close > stockDao.series[buyItem].close * 1.022)
                {
                    sold = true;
                }
                //Loss limit
                else if (stockDao.series[j].close < stockDao.series[buyItem].close * 0.992)
                {                    
                    sold = true;
                }
                //MFI sell signal???
                else if(indicatorDao.MoneyFlowIndex[j] < 50)
                {
                    sold = true;
                }

                //Next period is not on the same day as the open period 
                else if (origin.Day != jTime.Day)
                {
                    sold = true;
                }
                //End of data
                else if (j + 3 > stockDao.series.Count) 
                {
                    sold = true;
                }
            }
            return j;
        }
    }
}
