﻿using StockMarketResearch.Data.Storage;
using System;
using StockMarketResearch.Data.Base;

namespace StockMarketResearch.Strategies.IndicatorStrategies
{
    interface IIndicatorStrategy
    {
        IndicatorStrategyResultDao calculateResult();
        Boolean canOpenTrade(StockDao stockDao, IndicatorDao indicatorDao, int item, StockDao indexStockDao);
        int closeTrade(StockDao stockDao, IndicatorDao indicatorDao, int item);
    }
}
