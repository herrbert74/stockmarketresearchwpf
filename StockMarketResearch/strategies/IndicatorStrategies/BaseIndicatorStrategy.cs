﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using StockMarketResearch.strategies.rules;
using StockMarketResearch.Data.Storage;
using StockMarketResearch.Data.Yahoo;

namespace StockMarketResearch.Strategies.IndicatorStrategies
{
    abstract class BaseIndicatorStrategy : IIndicatorStrategy
    {
        public List<IBaseRule<RuleParameters>> rulesX = new List<IBaseRule<RuleParameters>>();

        YahooDataManager dataManager = new YahooDataManager();

        public IndicatorStrategyResultDao calculateResult()
        {
            IndicatorStrategyResultDao resultDao = new IndicatorStrategyResultDao();
            double win = 0;
            double loss = 0;
            resultDao.pos = 0;
            resultDao.pos = 0;
            resultDao.sum = 0;
            
            foreach (KeyValuePair<YahooDataManager.DataSetId, YahooDataSet> dataset in dataManager.datasets)
            {
                if (dataset.Key != YahooDataManager.DataSetId.INDEXES)
                {
                    foreach (string ticker in dataset.Value.universe.tickers)
                    {
                        string indicatorJson = File.ReadAllText(dataset.Value.analysisFolder + dataset.Value.setFolder + "//" + ticker + ".json");
                        IndicatorDao indicatorDao = JsonConvert.DeserializeObject<IndicatorDao>(indicatorJson);
                        string stockDaoJson = File.ReadAllText(dataset.Value.pricesFolder + dataset.Value.setFolder + ticker + ".json");
                        StockDao stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);

                        string indexTicker = dataset.Key == YahooDataManager.DataSetId.UK ? "^ftse" : "^gspc";
                        string indexStockJson = File.ReadAllText(dataManager.indexesDataSet.pricesFolder + "//indexes//" + indexTicker + ".json");
                        StockDao indexStockDao = JsonConvert.DeserializeObject<StockDao>(indexStockJson);
                        int closeItem = -1;
                        for (int buyItem = 0; buyItem < stockDao.series.Count - 10; buyItem++)
                        {
                            if (canOpenTrade(stockDao, indicatorDao, buyItem, indexStockDao) && buyItem > closeItem)
                            {
                                closeItem = closeTrade(stockDao, indicatorDao, buyItem);
                                if((stockDao.series[closeItem].close/ stockDao.series[buyItem].close) > 1.2)
                                {
                                    //nothing happens currently
                                }
                                resultDao.results.Add(new IndicatorStrategyResultDao.AnalysisResult(dataset.Key.ToString(), stockDao.ticker, stockDao.series[buyItem].Timestamp, stockDao.series[buyItem].close, stockDao.series[closeItem].Timestamp, stockDao.series[closeItem].close));
                                resultDao.sum += ((stockDao.series[closeItem].close / stockDao.series[buyItem].close) - 1 - Properties.Settings.Default.Spread);
                                if (stockDao.series[closeItem].close > stockDao.series[buyItem].close)
                                {
                                    resultDao.pos++;
                                    win += ((stockDao.series[closeItem].close / stockDao.series[buyItem].close) - 1 - Properties.Settings.Default.Spread);
                                    //System.Diagnostics.Debug.WriteLine(stockDao.series[closeItem].close / stockDao.series[buyItem].close);
                                }
                                else if (stockDao.series[closeItem].close < stockDao.series[buyItem].close)
                                {

                                    resultDao.neg++;
                                    loss += ((stockDao.series[closeItem].close / stockDao.series[buyItem].close) - 1 - Properties.Settings.Default.Spread);
                                }
                            }
                        }
                    }
                    
                }
            }
            resultDao.averageWin = win / resultDao.pos;
            resultDao.averageLoss = loss / resultDao.neg;
            resultDao.averageResult = (win + loss) / (resultDao.pos + resultDao.neg);
            resultDao.variance = getVariance(resultDao);
            resultDao.standardDeviation = Math.Sqrt(resultDao.variance) ;
            resultDao.sharpeRatio = resultDao.averageResult / resultDao.standardDeviation;
            return resultDao;
        }

        public bool validateAllRules(List<IBaseRule<RuleParameters>> rules)
        {
            foreach(IBaseRule<RuleParameters> rule in rules)
            {
                if (!rule.Validate()){
                    return false;
                }
            }
            return true;
        }

        double getVariance(IndicatorStrategyResultDao resultDao)
        {
            double squaredDifferences = 0;
            //int counter = 0;
            foreach (IndicatorStrategyResultDao.AnalysisResult analysisResult in resultDao.results)
            {
                //System.Diagnostics.Debug.WriteLine("sqDiff: " + (Math.Pow(((analysisResult.Close / analysisResult.Open) - resultDao.averageResult - 1.001), 2).ToString()));
                squaredDifferences += Math.Pow(((analysisResult.Close / analysisResult.Open) - resultDao.averageResult - 1.001), 2);
                //Console.Write(" " + squaredDifferences.ToString());
                //counter++;
            }
            System.Diagnostics.Debug.WriteLine(squaredDifferences.ToString());
            return squaredDifferences / resultDao.results.Count;
        }
        public abstract bool canOpenTrade(StockDao stockDao, IndicatorDao indicatorDao, int item, StockDao index);

        public abstract int closeTrade(StockDao stockDao, IndicatorDao indicatorDao, int item);
    }
}
