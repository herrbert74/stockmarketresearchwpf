﻿using System;
using StockMarketResearch.Data.Storage;

namespace StockMarketResearch.Strategies.IndicatorStrategies
{
    class MovingAverageShortStrategy : BaseIndicatorStrategy
    {
        public override bool canOpenTrade(StockDao stockDao, IndicatorDao indicatorDao, int item, StockDao indexIndicatorDAo)
        {
            if(stockDao.series[item].close < indicatorDao.ma20[item] && indicatorDao.ma20[item] < indicatorDao.ma50[item] && indicatorDao.ma50[item] < indicatorDao.ma200[item])
            {
                //Not a large gap
                if (stockDao.series[item].close > indicatorDao.ma20[item] * 1.2)
                {
                    //Volume is going to the righ direction
                    if (item > 10 && indicatorDao.OnVolumeBalance[item] < indicatorDao.OnVolumeBalance[item - 10]) {
                        return true;
                    }
                }
            }
            return false;
        }

        public override int closeTrade(StockDao stockDao, IndicatorDao indicatorDao, int item)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            origin.AddSeconds(stockDao.series[item].Timestamp);
            Boolean sold = false;
            int j = item + 1;
            while (!sold)
            {
                DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                jTime.AddSeconds(stockDao.series[j + 1].Timestamp);
                //Profit goal reached
                if (stockDao.series[j].close < stockDao.series[item].close * 0.984)
                {
                    sold = true;
                }
                //Loss limit
                else if (stockDao.series[j].close > stockDao.series[item].close * 1.006)
                {                    
                    sold = true;
                }
                //Next period is not on the same day as the open period 
                else if(origin.Day != jTime.Day)
                {
                    sold = true;
                }
                //End of data
                else if (j + 3 > stockDao.series.Count) 
                {
                    sold = true;
                }
                j++;

            }
            return j;
        }
    }
}
