﻿using StockMarketResearch.Data.Storage;
using System.Threading.Tasks;
using StockMarketResearch.Data.Base;
using System.Collections.Generic;
using Deedle;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Strategies.EarningsStrategies
{
    interface IEarningsStrategy
    {
        Task<EarningsStrategyResultDao> CalculateResult();
        BaseEarningsStrategy.CanOpenTradeResult CanOpenTrade(Series<long, StockPriceSeriesItem> series, EarningsDateItem o);
        List<long> CloseTrade(StockDao stockDao, BaseEarningsStrategy.CanOpenTradeResult canOpenTradeResult, EarningsDateItem o);
        EarningsStrategyParametersObject GetParameters();
    }
}
