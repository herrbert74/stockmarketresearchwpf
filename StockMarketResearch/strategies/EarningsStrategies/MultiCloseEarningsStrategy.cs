﻿using System;
using StockMarketResearch.strategies.rules;
using StockMarketResearch.Data.Storage;
using StockMarketResearch.Data.Base;
using System.Collections.Generic;
using StockMarketResearch.Utils;
using Deedle;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Strategies.EarningsStrategies
{
    class MultiCloseEarningsStrategy : BaseEarningsStrategy
    {
        
        public override CanOpenTradeResult CanOpenTrade(Series<long, StockPriceSeriesItem> series, EarningsDateItem o)
        {
            EarningsStrategyParametersInstance = GetParameters();
            rulesX.Clear();
            long resultDate = DateUtil.ConvertToTimestamp(DateUtil.ParseMySqlDate(o.ResultDate));
            if (DateUtil.ParseMySqlDate(o.ResultDate) > DateTime.Today.Date.AddSeconds(-1 * EarningsStrategyParametersInstance.MaxHoldingPeriod))                
            {
                return new CanOpenTradeResult(0, true);
            }
            double previousClose;
            try
            {
                previousClose = series.Before(resultDate).LastValue().close;
            }
            catch (Exception)
            {
                return new CanOpenTradeResult(0, true);
            }

            Series<long, StockPriceSeriesItem> SeriesAfterResultDate = series.After(resultDate);

            //There is a gap
            rulesX.Add(new LargerThanRule(SeriesAfterResultDate.GetAt(0).close, previousClose * 1.01));
            //half day later still gap
            rulesX.Add(new LargerThanRule(SeriesAfterResultDate.GetAt(EarningsStrategyParametersInstance.OpenInterval).close, previousClose * 1.01));

            CanOpenTradeResult CanOpenTradeResult = new CanOpenTradeResult();
            CanOpenTradeResult.IsLongTrade = true;
            if (validateAllRules(rulesX) > 0)
            {
                CanOpenTradeResult.OpenTimeStamp = SeriesAfterResultDate.GetAt(EarningsStrategyParametersInstance.OpenInterval).Timestamp;
            }
            else
            {
                CanOpenTradeResult.OpenTimeStamp = 0;
            }
            return CanOpenTradeResult;
        }

        public override List<long> CloseTrade(StockDao stockDao, CanOpenTradeResult canOpenTradeResult, EarningsDateItem o)
        {

            List<StockPriceSeriesItem> items = stockDao.GetSeriesListBetweenTimeStamps(canOpenTradeResult.OpenTimeStamp, canOpenTradeResult.OpenTimeStamp + EarningsStrategyParametersInstance.MaxHoldingPeriod);
            List<long> CloseTimeStamps = new List<long>();
            int LimitsHit = 0;
            foreach (StockPriceSeriesItem i in items)
            {
                //Stop hit
                if (i.close < items[0].close * (100 + EarningsStrategyParametersInstance.StopPercentage) / 100)
                {
                    CloseTimeStamps.Add(i.Timestamp);
                    return CloseTimeStamps;
                }
                //Limit hit
                else if (LimitsHit < EarningsStrategyParametersInstance.LimitPercentages.Length - 1)
                {
                    if (i.close > items[0].close * (100 + EarningsStrategyParametersInstance.LimitPercentages[LimitsHit]) / 100)
                    {
                        EarningsStrategyParametersInstance.StopPercentage = EarningsStrategyParametersInstance.LimitPercentages[LimitsHit] + EarningsStrategyParametersInstance.TrailingStopPercentage;
                        LimitsHit++;
                        CloseTimeStamps.Add(i.Timestamp);
                    }

                }

            }
            //Hit time limit
            CloseTimeStamps.Add(items[items.Count - 1].Timestamp);
            return CloseTimeStamps;
        }

        public override EarningsStrategyParametersObject GetParameters()
        {
            EarningsStrategyParametersObject o = new EarningsStrategyParametersObject();
            o.OpenInterval = 48;
            o.LimitPercentages = new double[] { 3, 6, 9, 12 };
            o.StopPercentage = -2;
            o.TrailingStopPercentage = -3;
            o.MaxHoldingPeriod = 10 * 24 * 60 * 60; // One week
            return o;
        }
    }
}
