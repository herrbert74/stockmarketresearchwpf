﻿using System;
using StockMarketResearch.strategies.rules;
using StockMarketResearch.Data.Storage;
using StockMarketResearch.Data.Base;
using System.Collections.Generic;
using StockMarketResearch.Utils;
using Deedle;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Strategies.EarningsStrategies
{
    class SimpleShortEarningsStrategy : BaseEarningsStrategy
    {

        public override CanOpenTradeResult CanOpenTrade(Series<long, StockPriceSeriesItem> series, EarningsDateItem o)
        {
            EarningsStrategyParametersInstance = GetParameters();
            rulesX.Clear();
            long resultDate = DateUtil.ConvertToTimestamp(DateUtil.ParseMySqlDate(o.ResultDate));
            if (resultDate > 1456790400)
            {
                return new CanOpenTradeResult(0, false);
            }
            double previousClose;
            try
            {
                previousClose = series.Before(resultDate).LastValue().close;
            }
            catch (Exception)
            {
                return new CanOpenTradeResult(0, false);
            }

            Series<long, StockPriceSeriesItem> SeriesAfterResultDate = series.After(resultDate);

            //There is a gap
            rulesX.Add(new LargerThanRule(previousClose * 0.99, SeriesAfterResultDate.GetAt(0).close));
            //half day later still gap
            rulesX.Add(new LargerThanRule(previousClose * 0.99, SeriesAfterResultDate.GetAt(EarningsStrategyParametersInstance.OpenInterval).close));
            CanOpenTradeResult CanOpenTradeResult = new CanOpenTradeResult();
            CanOpenTradeResult.IsLongTrade = false;
            if (validateAllRules(rulesX) > 0) {
                CanOpenTradeResult.OpenTimeStamp = SeriesAfterResultDate.GetAt(EarningsStrategyParametersInstance.OpenInterval).Timestamp;
            }
            else { 
                CanOpenTradeResult.OpenTimeStamp = 0;
            }
            return CanOpenTradeResult;
        }
        
        public override List<long> CloseTrade(StockDao stockDao, CanOpenTradeResult canOpenTradeResult, EarningsDateItem o)
        {
            
            List<StockPriceSeriesItem> items = stockDao.GetSeriesListBetweenTimeStamps(canOpenTradeResult.OpenTimeStamp, canOpenTradeResult.OpenTimeStamp + EarningsStrategyParametersInstance.MaxHoldingPeriod); 
            foreach (StockPriceSeriesItem i in items)
            {
                if (i.close > items[0].close * (100 + EarningsStrategyParametersInstance.StopPercentage) / 100)
                {
                    return new List<long> { i.Timestamp };
                }
            }
            return new List<long>{ items[items.Count - 1].Timestamp };
        }

        public override EarningsStrategyParametersObject GetParameters()
        {
            EarningsStrategyParametersObject o = new EarningsStrategyParametersObject();
            o.OpenInterval = 48;
            o.LimitPercentages = new double[] {1000}; //No limits :)
            o.StopPercentage = 2;
            o.MaxHoldingPeriod = 4 * 24 * 60 * 60; // One week
            return o;
        }
    }
}
