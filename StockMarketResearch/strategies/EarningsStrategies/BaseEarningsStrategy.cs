﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using StockMarketResearch.strategies.rules;
using StockMarketResearch.Data.Storage;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Network;
using System.Threading.Tasks;
using StockMarketResearch.Data.Base;
using StockMarketResearch.Utils;
using Deedle;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Strategies.EarningsStrategies
{
    abstract class BaseEarningsStrategy : IEarningsStrategy
    {
        public List<IBaseRule<RuleParameters>> rulesX = new List<IBaseRule<RuleParameters>>();

        public EarningsStrategyParametersObject EarningsStrategyParametersInstance = new EarningsStrategyParametersObject();

        YahooDataManager dataManager = new YahooDataManager();

        public async Task<EarningsStrategyResultDao> CalculateResult()
        {
            EarningsStrategyResultDao resultDao = new EarningsStrategyResultDao();
            double win = 0;
            double loss = 0;
            resultDao.pos = 0;
            resultDao.pos = 0;
            resultDao.sum = 0;

            List<EarningsDateItem> earningsDates = await DownloadUtil.DownloadEarningsDates();
            foreach (EarningsDateItem earningsDateObject in earningsDates)
            {
                System.Diagnostics.Debug.WriteLine("Company: " + earningsDateObject.Ticker);
                YahooDataSet dataset;
                dataManager.datasets.TryGetValue(earningsDateObject.Country, out dataset);

                string stockDaoJson = File.ReadAllText(dataset.pricesFolder + dataset.setFolder + earningsDateObject.Ticker + ".json");
                StockDao StockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
                long EarningsDate = DateUtil.ConvertToTimestamp(DateUtil.ParseMySqlDate(earningsDateObject.ResultDate));
                long PeriodStart = DateUtil.ConvertToTimestamp(DateUtil.ParseMySqlDate(earningsDateObject.ResultDate).AddDays(-7));
                long PeriodEnd = DateUtil.ConvertToTimestamp(DateUtil.ParseMySqlDate(earningsDateObject.ResultDate).AddDays(14));
                var Period = StockDao.GetSeriesBetweenTimeStamps(PeriodStart, PeriodEnd);

                var CanOpenTradeResult = CanOpenTrade(Period, earningsDateObject);
                List<long> CloseTimeStamps;
                if (CanOpenTradeResult.OpenTimeStamp > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Company started: " + earningsDateObject.Ticker);
                    CloseTimeStamps = CloseTrade(StockDao, CanOpenTradeResult, earningsDateObject);
                    
                    Dictionary<long, StockPriceSeriesItem> dictItems = StockDao.ConvertSeriesToDictionary();
                    List<double> ClosePrices = new List<double>();
                    for (int i = 0; i < CloseTimeStamps.Count; i++)
                    {
                        ClosePrices.Add(dictItems[CloseTimeStamps[i]].close);
                    }
                    
                    double TradeResult = GetTradeResult(CanOpenTradeResult, CloseTimeStamps, dictItems);
                    resultDao.results.Add(new EarningsStrategyResultDao.EarningsResult(dataset.ToString(), StockDao.ticker, StockDao.companyName, EarningsDate, CanOpenTradeResult.OpenTimeStamp, dictItems[CanOpenTradeResult.OpenTimeStamp].close, CloseTimeStamps, ClosePrices, Period, TradeResult));
                    
                    resultDao.sum += TradeResult;
                    if (TradeResult > 0)
                    {
                        resultDao.pos++;
                        win += TradeResult;
                        //System.Diagnostics.Debug.WriteLine(stockDao.series[closeItem].close / stockDao.series[buyItem].close);
                    }
                    else
                    {

                        resultDao.neg++;
                        loss += TradeResult;
                    }
                }
                else
                {
                    CloseTimeStamps = new List<long> { 0 };
                }
            }
            resultDao.results.Sort();
            resultDao.averageWin = win / resultDao.pos;
            resultDao.averageLoss = loss / resultDao.neg;
            resultDao.averageResult = (win + loss) / (resultDao.pos + resultDao.neg);
            resultDao.variance = getVariance(resultDao);
            resultDao.standardDeviation = Math.Sqrt(resultDao.variance);
            resultDao.sharpeRatio = resultDao.averageResult / resultDao.standardDeviation;
            return resultDao;
        }

        private double GetTradeResult(CanOpenTradeResult CanOpenTradeResult, List<long> CloseTimeStamps, Dictionary<long, StockPriceSeriesItem> dictItems)
        {
            double WeightedAverageClose;
            double SumClose = 0;
            for (int i = 0; i < CloseTimeStamps.Count; i++)
            {
                //Limit closes
                if (i < CloseTimeStamps.Count - 1)
                {
                    SumClose += dictItems[CloseTimeStamps[i]].close;
                }
                //Stop or time close
                else
                {
                    SumClose += (EarningsStrategyParametersInstance.LimitPercentages.Length + 2 - CloseTimeStamps.Count) * dictItems[CloseTimeStamps[i]].close;
                }
            }
            WeightedAverageClose = SumClose / (EarningsStrategyParametersInstance.LimitPercentages.Length + 1);
            if (CanOpenTradeResult.IsLongTrade)
            {


                return (WeightedAverageClose / dictItems[CanOpenTradeResult.OpenTimeStamp].close) - 1 - Properties.Settings.Default.Spread;
            }
            else
            {
                return (dictItems[CanOpenTradeResult.OpenTimeStamp].close / WeightedAverageClose) - 1 - Properties.Settings.Default.Spread;
            }
        }

        /*private static bool IsTradeWinning(CanOpenTradeResult CanOpenTradeResult, List<long> CloseTimeStamps, Dictionary<long, DataSeriesItem> dictItems)
        {
            //XNOR gate! The trade is winning if the close price is greater than the open price and the trade was long or it is less and it was a short trade.
            return dictItems[CloseTimeStamps[0]].close > dictItems[CanOpenTradeResult.OpenTimeStamp].close == CanOpenTradeResult.IsLongTrade;
        }*/

        public long validateAllRules(List<IBaseRule<RuleParameters>> rules)
        {
            foreach (IBaseRule<RuleParameters> rule in rules)
            {
                if (!rule.Validate())
                {
                    return 0;
                }
            }
            return 1;
        }

        double getVariance(EarningsStrategyResultDao resultDao)
        {
            double squaredDifferences = 0;
            //int counter = 0;
            foreach (EarningsStrategyResultDao.EarningsResult analysisResult in resultDao.results)
            {
                //System.Diagnostics.Debug.WriteLine("sqDiff: " + (Math.Pow(((analysisResult.Close / analysisResult.Open) - resultDao.averageResult - 1.001), 2).ToString()));
                squaredDifferences += Math.Pow(((analysisResult.ClosePrices[0] / analysisResult.Open) - resultDao.averageResult - 1.001), 2);
                //Console.Write(" " + squaredDifferences.ToString());
                //counter++;
            }
            System.Diagnostics.Debug.WriteLine(squaredDifferences.ToString());
            return squaredDifferences / resultDao.results.Count;
        }

        public abstract EarningsStrategyParametersObject GetParameters();

        public abstract CanOpenTradeResult CanOpenTrade(Series<long, StockPriceSeriesItem> series, EarningsDateItem o);

        public abstract List<long> CloseTrade(StockDao stockDao, CanOpenTradeResult canOpenTradeResult, EarningsDateItem o);

        public class CanOpenTradeResult
        {
            public CanOpenTradeResult() { }

            public CanOpenTradeResult(long openTimaStamp, bool isLongTrade)
            {
                OpenTimeStamp = openTimaStamp;
                IsLongTrade = isLongTrade;
            }
            public long OpenTimeStamp { get; set; }
            public bool IsLongTrade { get; set; }
        }
    }

}
