﻿namespace StockMarketResearch.Strategies.EarningsStrategies
{
    public class EarningsStrategyParametersObject
    {
        public int OpenInterval { get; set; }
        public double[] LimitPercentages { get; set; }
        public double StopPercentage { get; set; }
        public double TrailingStopPercentage { get; set; }
        public long MaxHoldingPeriod { get; set; }
    }
}
