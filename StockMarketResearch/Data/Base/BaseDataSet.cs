﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Data.Base
{
    public class BaseDataSet
    {
        public string name;
        public string hostName;
        public string endPoint;
        public Universe universe;
        public string baseDataFolder;
        public string pricesFolder;
        public string analysisFolder;
        public string cyclesFolder;
        public string setFolder;
        //TODO Move these to QuandlDataSet if needed
        //public String quanldSource;
        //public String quandlPrefix;
    }
}
