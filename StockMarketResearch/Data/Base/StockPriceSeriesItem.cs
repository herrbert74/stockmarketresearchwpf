﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Data.Base
{
    public class StockPriceSeriesItem
    {
        public long Timestamp;
        public double close;
        public double high;
        public double low;
        public double open;
        public int volume;

    }
}
