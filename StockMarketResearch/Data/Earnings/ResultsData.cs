﻿using StockMarketResearch.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Data
{
    class ResultsDataItem
    {
        public long date; //Unix Timestamp
        public string ticker;
        public string index;

        public ResultsDataItem(long v1, string v2, string v3)
        {
            this.date = v1;
            this.ticker = v2;
            this.index = v3;
        }
    }

    class CompanyResultsData
    {
        public List<ResultsDataItem> results;
        
        public CompanyResultsData()
        {
            results = new List<ResultsDataItem>();
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("26/10/2015")), "WPP.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("27/10/2015")), "STJ.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("28/10/2015")), "NEX.L", "ftse250"));

            //UK Week Beginning 2/11/2015
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "IMT.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "ABF.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "STAN.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "DLG.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "JE.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "AMFW.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "WEIR.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "INDV.L", "ftse250"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("03/11/2015")), "PDG.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("04/11/2015")), "GLEN.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("04/11/2015")), "LGEN.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("04/11/2015")), "MKS.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("04/11/2015")), "WIZZ.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("04/11/2015")), "OSB.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("04/11/2015")), "JDW.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "AZN.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "EZJ.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "CCH.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "RRS.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "MRW.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "CWC.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "COB.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "HWDN.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "RTO.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "TATE.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "KWE.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "LRE.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "SGP.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("05/11/2015")), "JRG.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("06/11/2015")), "IAG.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("06/11/2015")), "ISAT.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("06/11/2015")), "3IN.L", "ftse250"));
            //US Week Beginning 2/11/2015
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "V", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "AIG", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "D", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "ECL", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "EL", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "CAH", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "GGP", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "ALL", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "SYY", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "PXD", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "VNO", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "ES", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "CLX", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "NBL", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "L", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("02/11/2015")), "ETR", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("02/11/2015")), "CHD", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("02/11/2015")), "CNA", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("02/11/2015")), "EEP", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("02/11/2015")), "TW", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("02/11/2015")), "FIT", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("02/11/2015")), "SSNC", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("02/11/2015")), "ACSF", "sp500"));

            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("03/11/2015")), "ATVI", "sp500"));
            
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "HSX.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "AGK.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "RDW.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "DTY.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "VOD.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "PRU.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "NG.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "EXPN.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "LAND.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "ITV.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "WOS.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "DCC.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "BTG.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "AVV.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("10/11/2015")), "DRX.L", "ftse250"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("11/11/2015")), "SSE.L", "ftse100"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "PCLN", "sp500"));
            //results.Add(new ResultsDataItem(DateUtils.ConvertToTimestamp(DateUtils.parseDate("09/11/2015")), "DISH", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "AAL", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "UAL", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "GPS", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "AMG", "sp500"));
            results.Add(new ResultsDataItem(DateUtil.ConvertToTimestamp(DateUtil.ParseDate("09/11/2015")), "IFF", "sp500"));
        }
    }
}
