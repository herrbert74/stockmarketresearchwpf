﻿using StockMarketResearch.Data.Base;
using System.Collections.Generic;

namespace StockMarketResearch.Data.Network
{
    public class BaseStockResponseDto
    {
        public List<StockPriceSeriesItem> series;
    }
}