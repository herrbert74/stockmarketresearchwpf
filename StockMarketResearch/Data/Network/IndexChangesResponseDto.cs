﻿
namespace StockMarketResearch.Data.Network
{
    class IndexChangesResponseDto
    {
        public long id { get; set; }
        public int ticker_id { get; set; }
        public string date { get; set; }
        public int index_id { get; set; }
        public bool addition { get; set; }

    }
}
