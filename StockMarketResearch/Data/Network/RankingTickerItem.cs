﻿namespace StockMarketResearch.Data.Network
{
    class RankingTickerItem : TickerItem
    {
        public double price;
        public double marketCap;
        public int index;

        public RankingTickerItem()
        {

        }

        public RankingTickerItem(int v1, string v2, string v3, string v4, double v5, double v6, int index)
        {
            this.id = v1;
            this.Ticker = v2;
            this.Name = v3;
            this.IssuedShares = v4;
            this.price = v5;
            this.marketCap = v6;
            this.index = index;
        }
    }
}
