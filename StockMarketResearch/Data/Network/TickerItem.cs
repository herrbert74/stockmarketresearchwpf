﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Data.Network
{
    public class TickerItem
    {
        public int id;
        public string Ticker;
        public string Name;
        public string IssuedShares;
        public string FourTraderslink;
    }
}
