﻿using StockMarketResearch.Data.Yahoo;

namespace StockMarketResearch.Data.Network
{
    public class EarningsDateItem
    {
        public string ResultDate;
        public string Ticker;
        public string Description;
        public YahooDataManager.DataSetId Country;
    }
}
