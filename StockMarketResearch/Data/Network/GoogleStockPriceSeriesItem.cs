﻿using StockMarketResearch.Data.Base;

namespace StockMarketResearch.Data.Network
{
    public class GoogleStockPriceSeriesItem : StockPriceSeriesItem
    {
        public string Date;
    }
}
