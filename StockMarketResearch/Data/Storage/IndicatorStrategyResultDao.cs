﻿using System.Collections.Generic;

namespace StockMarketResearch.Data.Storage
{
    public class IndicatorStrategyResultDao : BaseResultDao
    {
        
        public List<AnalysisResult> results = new List<AnalysisResult>();

        public class AnalysisResult
        {
            public string Country;
            public string Ticker;
            public long OpenTimeStamp;
            public double Open;
            public long CloseTimeStamp;
            public double Close;

            public AnalysisResult(string Country, string Ticker, long OpenTimeStamp, double open, long CloseTimeStamp, double close)
            {
                this.Country = Country;
                this.Ticker = Ticker;
                this.OpenTimeStamp = OpenTimeStamp;
                this.Open = open;
                this.CloseTimeStamp = CloseTimeStamp;
                this.Close = close;

            }
        }
    }
}
