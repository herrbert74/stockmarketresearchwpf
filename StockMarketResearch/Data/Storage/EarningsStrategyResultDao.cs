﻿using Deedle;
using StockMarketResearch.Data.Base;
using System;
using System.Collections.Generic;

namespace StockMarketResearch.Data.Storage
{
    public class EarningsStrategyResultDao : BaseResultDao
    {
        
        public List<EarningsResult> results = new List<EarningsResult>();

        public class EarningsResult : IComparable<EarningsResult>
        {
            public string Country;
            public string Ticker;
            public string Company;
            public long StartTimeStamp;
            public long OpenTimeStamp;
            public double Open;
            public List<long> CloseTimeStamps;
            public List<double> ClosePrices;
            public Series<long, StockPriceSeriesItem> Series;
            public double TradeResult;

            public EarningsResult(string country, string ticker, string company, long startTimeStamp, long openTimeStamp, double open, List<long> closeTimeStamps, List<double> closePrices, Series<long, StockPriceSeriesItem> series, double tradeResult)
            {
                Country = country;
                Ticker = ticker;
                Company = company;
                StartTimeStamp = startTimeStamp;
                OpenTimeStamp = openTimeStamp;
                Open = open;
                CloseTimeStamps = closeTimeStamps;
                ClosePrices = closePrices;
                Series = series;
                TradeResult = tradeResult;
            }

            public int CompareTo(EarningsResult other)
            {
                return (OpenTimeStamp.CompareTo(other.OpenTimeStamp));
            }
        }
    }
}
