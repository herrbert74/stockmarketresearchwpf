﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Data.Storage
{
    public class BaseResultDao
    {
        public int pos;
        public int neg;
        public double sum;
        public double averageWin;
        public double averageLoss;
        public double averageResult;
        public double variance;
        public double standardDeviation;
        public double sharpeRatio;
    }
}
