﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Data.Storage
{
    public class IndicatorDao
    {
        public string ticker;
        public List<double> ma200;
        public List<double> ma50;
        public List<double> ma20;
        public List<long> OnVolumeBalance;
        public List<double> MoneyFlowIndex;
    }
}
