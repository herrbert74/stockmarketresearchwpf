﻿using Deedle;
using StockMarketResearch.Data.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StockMarketResearch.Data.Storage
{

    public class StockDao


    {
        public string ticker;
        public string companyName;
        public string exchangeName;
        public List<StockPriceSeriesItem> series = new List<StockPriceSeriesItem>();


        //http://bluemountaincapital.github.io/Deedle/index.html

        public Dictionary<long, StockPriceSeriesItem> ConvertSeriesToDictionary()
        {
            return series
                .GroupBy(p => p.Timestamp)
                .ToDictionary(g => g.Key, g => g.First());
        }

        public List<StockPriceSeriesItem> GetSeriesListBetweenTimeStamps(long start, long finish)
        {
            Series<long, StockPriceSeriesItem> sliced = GetSeriesBetweenTimeStamps(start, finish);
            List<StockPriceSeriesItem> items = new List<StockPriceSeriesItem>();
            foreach (KeyValuePair<long, StockPriceSeriesItem> i in sliced.Observations)
            {
                items.Add(i.Value);

            }
            return items;
        }

        public Series<long, StockPriceSeriesItem> GetSeriesBetweenTimeStamps(long start, long finish)
        {
            Dictionary<long, StockPriceSeriesItem> d = ConvertSeriesToDictionary();

            Series<long, StockPriceSeriesItem> s = d.ToSeries();

            var sliced = s.Between(start, finish);
            return sliced;
        }

        public void addNewStockResponse(List<StockPriceSeriesItem> responseSeries)
        {

            //Delete items after a timestamp
            //*************** USE THIS ONLY IF WANT TO REMOVE ITEMS AFTER A GAP IN THE DATA. COMMENT IT OUT OTHERWISE. ******************

            /*long deleteAfter = 1453131502;
            List<DataSeriesItem> copySeries = new List<DataSeriesItem>(series);

            foreach (DataSeriesItem item in series)
            {
                if (item.Timestamp > deleteAfter)
                {
                    copySeries.Remove(item);
                }
            }

            series = copySeries;*/

            //*************** USE THIS ONLY IF WANT TO REMOVE ITEMS AFTER A GAP IN THE DATA. COMMENT IT OUT OTHERWISE. ******************

            long latestTimeStamp;
            if (series.Count > 0)
            {
                latestTimeStamp = series[series.Count - 1].Timestamp;
            }
            else
            {
                latestTimeStamp = 0;
            }

            
            if (responseSeries != null)
            {
                

                foreach (StockPriceSeriesItem item in responseSeries)
                {
                    if (item.Timestamp > latestTimeStamp)
                    {
                        series.Add(item);
                    }
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Ticker with null series found! " + ticker);
            }
        }

        

        
    }



}
