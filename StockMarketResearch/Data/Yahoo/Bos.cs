﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Text.RegularExpressions;

namespace StockMarketResearch.Data.Yahoo
{
    public class Meta
    {
        public string uri;
        public string ticker;
        [JsonProperty("Company-Name")]
        public string CompanyName;
        [JsonProperty("Exchange-Name")]
        public String ExchangeName;
        public string unit;
        public string timezone;
        public string currency;
        public int gmtoffset;
        public double previous_close;
    }

    public class PriceRange
    {
        public double min;
        public double max;
    }

    public class PriceRanges
    {
        public PriceRange close;
        public PriceRange high;
        public PriceRange low;
        public PriceRange open;
    }

    public class TimeRange
    {
        public long min;
        public long max;
    }

    public class TimeStampRange : TimeRange
    {
        public int date;
    }

    public class VolumeRange
    {
        public int min;
        public int max;
    }

    public class StockResponseConverter : CustomCreationConverter<YahooStockResponse>
    {
        public override YahooStockResponse Create(Type objectType)
        {
            throw new NotImplementedException();
        }

        /*public override StockResponse Create(Type objectType)
{
   return new StockResponse();
}*/

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string newBody = Regex.Replace(existingValue.ToString(), "finance_charts_json_callback\\(", "");
            newBody = Regex.Replace(newBody, "\\)$", "");
            return base.ReadJson(reader, objectType, newBody, serializer);
        }


    }


}
