﻿using Newtonsoft.Json;
using StockMarketResearch.Data.Base;
using StockMarketResearch.Data.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using StockMarketResearch.Network;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Data.Yahoo
{
    public class YahooDataManager
    {
        public enum DataSetId {INDEXES, UK, US};
        static Universe indexesUniverse = new Universe(new string[] { "^ftse", "^ftmc", "^gspc" });
        //static Universe ukUniverse = new Universe(new string[] { "AAL.L", "ABF.L", "ADM.L", "ADN.L", "AHT.L", "ANTO.L", "ARM.L", "AV.L", "AZN.L", "BA.L", "BAB.L", "BARC.L", "BATS.L", "BDEV.L", "BG.L", "BLND.L", "BLT.L", "BNZL.L", "BP.L", "BRBY.L", "BT-A.L", "CCH.L", "CCL.L", "CNA.L", "CPG.L", "CPI.L", "CRH.L", "DC.L", "DGE.L", "DLG.L", "EXPN.L", "EZJ.L", "FRES.L", "GFS.L", "GKN.L", "GLEN.L", "GSK.L", "HIK.L", "HL.L", "HMSO.L", "HSBA.L", "IAG.L", "IHG.L", "III.L", "IMT.L", "INTU.L", "ISAT.L", "ITRK.L", "ITV.L", "JMAT.L", "KGF.L", "LAND.L", "LGEN.L", "LLOY.L", "LSE.L", "MERL.L", "MGGT.L", "MKS.L", "MNDI.L", "MRW.L", "NG.L", "NXT.L", "OML.L", "PRU.L", "PSN.L", "PSON.L", "RB.L", "RBS.L", "RDSA.L", "RDSB.L", "REL.L", "RIO.L", "RMG.L", "RR.L", "RRS.L", "RSA.L", "SAB.L", "SBRY.L", "SDR.L", "SGE.L", "SHP.L", "SKY.L", "SL.L", "SMIN.L", "SN.L", "SPD.L", "SSE.L", "STAN.L", "STJ.L", "SVT.L", "TPK.L", "TSCO.L", "TUI.L", "TW.L", "ULVR.L", "UU.L", "VOD.L", "WEIR.L", "WOS.L", "WPP.L", "WTB.L", "3IN.L", "AA.L", "ACA.L", "AGK.L", "ALD.L", "ALM.L", "ALNT.L", "AMFW.L", "AML.L", "ANH.L", "AO.L", "ASHM.L", "ASL.L", "ATK.L", "ATST.L", "AUTO.L", "AVV.L", "BABS.L", "BAG.L", "BBA.L", "BBOX.L", "BBY.L", "BET.L", "BEZ.L", "BGEO.L", "BHMG.L", "BKG.L", "BME.L", "BNKR.L", "BOK.L", "BOY.L", "BPTY.L", "BRSN.L", "BRW.L", "BTEM.L", "BTG.L", "BVIC.L", "BVS.L", "BWNG.L", "BWY.L", "BYG.L", "CAPC.L", "CARD.L", "CBG.L", "CCC.L", "CEY.L", "CHOO.L", "CINE.L", "CKN.L", "CLDN.L", "CLI.L", "CLLN.L", "CNE.L", "COB.L", "CRDA.L", "CRST.L", "CTY.L", "CWC.L", "CWD.L", "CWK.L", "DCC.L", "DCG.L", "DEB.L", "DJAN.L", "DLN.L", "DNLM.L", "DOM.L", "DPH.L", "DPLM.L", "DRX.L", "DTY.L", "ECM.L", "EDIN.L", "ELM.L", "ELTA.L", "EMG.L", "ERM.L", "ESNT.L", "ESUR.L", "ETI.L", "ETO.L", "EVR.L", "FCPT.L", "FCSS.L", "FDSA.L", "FEV.L", "FGP.L", "FGT.L", "FOXT.L", "FRCL.L", "FSJ.L", "GCP.L", "GFRD.L", "GFTU.L", "GNC.L", "GNK.L", "GNS.L", "GOG.L", "GPOR.L", "GRG.L", "GRI.L", "GSS.L", "HAS.L", "HFD.L", "HGG.L", "HICL.L", "HLMA.L", "HOME.L", "HSTN.L", "HSV.L", "HSX.L", "HTG.L", "HTY.L", "HWDN.L", "IAP.L", "ICP.L", "IGG.L", "IMI.L", "INCH.L", "INDV.L", "INF.L", "INPP.L", "INVP.L", "IPF.L", "IPO.L", "IRV.L", "JAM.L", "JD.L", "JDW.L", "JE.L", "JLG.L", "JLIF.L", "JLT.L", "JMG.L", "JRG.L", "JUP.L", "KAZ.L", "KIE.L", "KLR.L", "KWE.L", "LAD.L", "LMI.L", "LMP.L", "LOOK.L", "LRD.L", "LRE.L", "MAB.L", "MARS.L", "MCRO.L", "MGAM.L", "MLC.L", "MNKS.L", "MONY.L", "MPI.L", "MRC.L", "MRO.L", "MSLH.L", "MTO.L", "MYI.L", "NBLS.L", "NEX.L", "NMC.L", "NOG.L", "NTG.L", "OCDO.L", "OPHR.L", "OSB.L", "PAG.L", "PAY.L", "PCT.L", "PDL.L", "PETS.L", "PFC.L", "PFG.L", "PFL.L", "PHNX.L", "PIC.L", "PLI.L", "PLND.L", "PMO.L", "PNN.L", "POLY.L", "PTEC.L", "PZC.L", "QQ.L", "RAT.L", "RCP.L", "RDI.L", "RDW.L", "REX.L", "RGU.L", "RMV.L", "RNK.L", "ROR.L", "RPC.L", "RSE.L", "RSW.L", "RTN.L", "RTO.L", "SAGA.L", "SCIN.L", "SGC.L", "SGP.L", "SGRO.L", "SHAW.L", "SHB.L", "SHI.L", "SMDS.L", "SMP.L", "SMT.L", "SMWH.L", "SNR.L", "SPI.L", "SPX.L", "SRP.L", "SSPG.L", "SVI.L", "SVS.L", "SXS.L", "SYNT.L", "SYR.L", "TALK.L", "TATE.L", "TCG.L", "TCY.L", "TED.L", "TEM.L", "TEP.L", "TLPR.L", "TLW.L", "TMPL.L", "TRY.L", "UBM.L", "UDG.L", "UKCM.L", "ULE.L", "UTG.L", "VCT.L", "VEC.L", "VED.L", "VM.L", "VSVS.L", "WG.L", "WIZZ.L", "WKP.L", "WMH.L", "WPCT.L", "WTAN.L", "WWH.L", "ZPLA.L", "SOPH.L", "CIR.L", "P2P.L", "P2P2.L", "WPG.L", "HSTG.L", "AGR.L", "IBST.L", "TRIG.L", "NCC.L", "SAFE.L" });
        static Universe usUniverse = new Universe(new string[] { "MMM", "ABT", "ABBV", "ACN", "ACE", "ATVI", "ADBE", "ADT", "AAP", "AES", "AET", "AFL", "AMG", "A", "GAS", "APD", "ARG", "AKAM", "AA", "AGN", "ALXN", "ALLE", "ADS", "ALL", "ALTR", "MO", "AMZN", "AEE", "AAL", "AEP", "AXP", "AIG", "AMT", "AMP", "ABC", "AME", "AMGN", "APH", "APC", "ADI", "AON", "APA", "AIV", "AAPL", "AMAT", "ADM", "AIZ", "T", "ADSK", "ADP", "AN", "AZO", "AVGO", "AVB", "AVY", "BHI", "BLL", "BAC", "BK", "BCR", "BXLT", "BAX", "BBT", "BDX", "BBBY", "BRK-B", "BBY", "BIIB", "BLK", "HRB", "BA", "BWA", "BXP", "BSX", "BMY", "BRCM", "BF-B", "CHRW", "CA", "CVC", "COG", "CAM", "CPB", "COF", "CAH", "HSIC", "KMX", "CCL", "CAT", "CBG", "CBS", "CELG", "CNP", "CTL", "CERN", "CF", "SCHW", "CHK", "CVX", "CMG", "CB", "CI", "XEC", "CINF", "CTAS", "CSCO", "C", "CTXS", "CLX", "CME", "CMS", "COH", "KO", "CCE", "CTSH", "CL", "CPGX", "CMCSA", "CMA", "CSC", "CAG", "COP", "CNX", "ED", "STZ", "GLW", "COST", "CCI", "CSX", "CMI", "CVS", "DHI", "DHR", "DRI", "DVA", "DE", "DLPH", "DAL", "XRAY", "DVN", "DO", "DFS", "DISCA", "DISCK", "DG", "DLTR", "D", "DOV", "DOW", "DPS", "DTE", "DD", "DUK", "DNB", "ETFC", "EMN", "ETN", "EBAY", "ECL", "EIX", "EW", "EA", "EMC", "EMR", "ENDP", "ESV", "ETR", "EOG", "EQT", "EFX", "EQIX", "EQR", "ESS", "EL", "ES", "EXC", "EXPE", "EXPD", "ESRX", "XOM", "FFIV", "FB", "FAST", "FDX", "FIS", "FITB", "FSLR", "FE", "FISV", "FLIR", "FLS", "FLR", "FMC", "FTI", "F", "FOSL", "BEN", "FCX", "FTR", "GME", "GPS", "GRMN", "GD", "GE", "GGP", "GIS", "GM", "GPC", "GNW", "GILD", "GS", "GT", "GOOGL", "GOOG", "GWW", "HAL", "HBI", "HOG", "HAR", "HRS", "HIG", "HAS", "HCA", "HCP", "HCN", "HP", "HES", "HPQ", "HD", "HON", "HRL", "HST", "HCBK", "HUM", "HBAN", "ITW", "IR", "INTC", "ICE", "IBM", "IP", "IPG", "IFF", "INTU", "ISRG", "IVZ", "IRM", "JEC", "JBHT", "JNJ", "JCI", "JOY", "JPM", "JNPR", "KSU", "K", "KEY", "GMCR", "KMB", "KIM", "KMI", "KLAC", "KSS", "KHC", "KR", "LB", "LLL", "LH", "LRCX", "LM", "LEG", "LEN", "LVLT", "LUK", "LLY", "LNC", "LLTC", "LMT", "L", "LOW", "LYB", "MTB", "MAC", "M", "MNK", "MRO", "MPC", "MAR", "MMC", "MLM", "MAS", "MA", "MAT", "MKC", "MCD", "MHFI", "MCK", "MJN", "WRK", "MDT", "MRK", "MET", "KORS", "MCHP", "MU", "MSFT", "MHK", "TAP", "MDLZ", "MON", "MNST", "MCO", "MS", "MOS", "MSI", "MUR", "MYL", "NDAQ", "NOV", "NAVI", "NTAP", "NFLX", "NWL", "NFX", "NEM", "NWSA", "NEE", "NLSN", "NKE", "NI", "NBL", "JWN", "NSC", "NTRS", "NOC", "NRG", "NUE", "NVDA", "ORLY", "OXY", "OMC", "OKE", "ORCL", "OI", "PCAR", "PH", "PDCO", "PAYX", "PYPL", "PNR", "PBCT", "POM", "PEP", "PKI", "PRGO", "PFE", "PCG", "PM", "PSX", "PNW", "PXD", "PBI", "PCL", "PNC", "RL", "PPG", "PPL", "PX", "PCP", "PCLN", "PFG", "PG", "PGR", "PLD", "PRU", "PEG", "PSA", "PHM", "PVH", "QRVO", "PWR", "QCOM", "DGX", "RRC", "RTN", "O", "RHT", "REGN", "RF", "RSG", "RAI", "RHI", "ROK", "COL", "ROP", "ROST", "RCL", "R", "CRM", "SNDK", "SCG", "SLB", "SNI", "STX", "SEE", "SRE", "SHW", "SIAL", "SIG", "SPG", "SWKS", "SLG", "SJM", "SNA", "SO", "LUV", "SWN", "SE", "STJ", "SWK", "SPLS", "SBUX", "HOT", "STT", "SRCL", "SYK", "STI", "SYMC", "SYY", "TROW", "TGT", "TEL", "TE", "TGNA", "THC", "TDC", "TSO", "TXN", "TXT", "HSY", "TRV", "TMO", "TIF", "TWX", "TWC", "TJX", "TMK", "TSS", "TSCO", "RIG", "TRIP", "FOXA", "TSN", "TYC", "USB", "UA", "UNP", "UAL", "UNH", "UPS", "URI", "UTX", "UHS", "UNM", "URBN", "VFC", "VLO", "VAR", "VTR", "VRSN", "VZ", "VRTX", "VIAB", "V", "VNO", "VMC", "WMT", "WBA", "DIS", "WM", "WAT", "ANTM", "WFC", "WDC", "WU", "WY", "WHR", "WFM", "WMB", "WEC", "WYN", "WYNN", "XEL", "XRX", "XLNX", "XL", "XYL", "YHOO", "YUM", "ZBH", "ZION", "ZTS", "AAP", "PYPL", "SIG", "ATVI", "UAL", "VRSK", "HPE", "SYF", "ILMN", "CSRA" });
        public static HashSet<string> nasdaqUniverse = new HashSet<string>(new string[] { "SNDK", "PCLN", "FB", "COST", "AMZN", "AMGN", "CELG", "AVGO", "GOOG", "REGN", "BIIB", "ALXN", "GOOGL", "EXPE", "MNST", "ILMN", "FISV", "INTU", "FFIV", "SRCL", "ORLY", "MU", "HSIC", "CSCO", "MSFT", "AAPL", "INTC", "SBUX", "EQIX", "NFLX", "AMAT", "NVDA", "AAL", "YHOO", "QCOM", "DISCK", "ATVI", "CMCSA", "FOXA", "WFM", "CSX", "STX", "EBAY", "FSLR", "SYMC", "GILD", "NAVI", "MDLZ", "WDC", "PBCT", "PYPL", "FITB", "DISCA", "CA", "CTSH", "ETFC", "NTAP", "MAT", "WBA", "GT", "ENDP", "PAYX", "ZION", "FAST", "TXN", "NWSA", "VIAB", "KHC", "SWKS", "MCHP", "MYL", "ESRX", "WYNN", "LLTC", "MAR", "CERN", "ADBE", "URBN", "ADP", "ROST", "CHRW", "XLNX", "LRCX", "BBBY", "PCAR", "TRIP", "GRMN", "ADI", "EA", "AKAM", "DLTR", "TROW", "CINF", "NTRS", "QRVO", "CME", "EXPD", "HAS", "CTXS", "KLAC", "VRTX", "TSCO", "ADSK", "PDCO", "XRAY", "FOSL", "NDAQ", "JBHT", "SNI", "VRSN", "CTAS", "FLIR", "VRSK", "HBAN", "FTR", "SPLS", "ISRG" });
        public YahooDataSet indexesDataSet = new YahooDataSet("//indexes//", indexesUniverse);
        public YahooDataSet ukDataSet;// = new YahooDataSet("//uk//", ukUniverse);
        public YahooDataSet usDataSet = new YahooDataSet("//us//", usUniverse);

        public Dictionary<DataSetId, YahooDataSet> datasets = new Dictionary<DataSetId, YahooDataSet>();
        
        public YahooDataManager()
        {
            datasets.Add(DataSetId.INDEXES, indexesDataSet);
            //datasets.Add(DataSetId.UK, ukDataSet);
            datasets.Add(DataSetId.US, usDataSet);
            Task.Run(() => this.DownloadUkTickerList()).Wait();
            
        }

        private async Task DownloadUkTickerList()
        {
            List<TickerItem> tickerList = await DownloadUtil.DownloadTickerList();

            string[] ukTickerArray = new string[tickerList.Count];
            int i = 0;
            foreach (TickerItem item in tickerList)
            {
                ukTickerArray[i] = item.Ticker;
                i++;
            }
            datasets.Add(YahooDataManager.DataSetId.UK, new YahooDataSet("//uk//", new Universe(ukTickerArray)));
        }

        public void convertDtoToDao(DataSetId id, string universeName)
        {

            foreach (string ticker in datasets[id].universe.tickers)
            {
                string dtoJson = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "//stockdata_ori//" + universeName + "//" + ticker + ".json");
                dtoJson = Regex.Replace(dtoJson, "\\)$", "");
                YahooStockResponse stockResponse = JsonConvert.DeserializeObject<YahooStockResponse>(dtoJson);
                StockDao stockDao = convertDtoToDao(stockResponse);
                string json = JsonConvert.SerializeObject(stockDao, Formatting.Indented); ;
                if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "//stockdata//" + universeName))
                {
                    Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "//stockdata//" + universeName);
                }
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "//stockdata//" + universeName + "//" + ticker + ".json", json);
            }

        }

        public StockDao convertDtoToDao(YahooStockResponse stockResponse)
        {
            StockDao result = new StockDao();
            result.companyName = stockResponse.meta.CompanyName;
            result.exchangeName = stockResponse.meta.ExchangeName;
            result.ticker = stockResponse.meta.ticker;
            result.series = stockResponse.series;
            return result;
        }

        public void updateDao(DataSetId id, YahooStockResponse stockResponse)
        {
            string ticker = stockResponse.meta.ticker;
            StockDao stockDao;
            if (File.Exists(datasets[id].pricesFolder + datasets[id].setFolder + ticker + ".json"))
            {
                string dtoJson = File.ReadAllText(datasets[id].pricesFolder + datasets[id].setFolder + ticker + ".json");
                stockDao = JsonConvert.DeserializeObject<StockDao>(dtoJson);
                stockDao.addNewStockResponse(stockResponse.series);
            }
            else
            {
                stockDao = convertDtoToDao(stockResponse);
            }
            string jsonStockDao = JsonConvert.SerializeObject(stockDao, Formatting.Indented);
            //System.Diagnostics.Debug.WriteLine(datasets[id].pricesFolder + datasets[id].setFolder + ticker.ToUpper() + ".json");
            try
            {
                File.WriteAllText(datasets[id].pricesFolder + datasets[id].setFolder + ticker.ToUpper() + ".json", jsonStockDao);
            }catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + " " + ticker);
            }

        }

    }
}
