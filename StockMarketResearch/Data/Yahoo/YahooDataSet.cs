﻿using StockMarketResearch.Data.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Data.Yahoo
{
    public class YahooDataSet : BaseDataSet
    {
        public YahooDataSet(string setFolder, Universe universe)
        {
            this.baseDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "//stockdata";
            this.analysisFolder = baseDataFolder + "//analysis";
            this.pricesFolder = baseDataFolder + "//prices";
            this.cyclesFolder = baseDataFolder + "//cycles";
            this.setFolder = setFolder;
            hostName = "http://chartapi.finance.yahoo.com";
            endPoint = "/instrument/1.0/{ticker}/chartdata;type=quote;range={days}d/json";
            this.universe = universe;
        }
    }
}
