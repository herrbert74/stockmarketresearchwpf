﻿using Newtonsoft.Json;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Data.Yahoo
{
    public class YahooStockResponse : BaseStockResponseDto
    {
        public Meta meta;
        [JsonProperty("TimeStamp-Ranges")]
        public TimeStampRange[] TimeStampRanges;
        public TimeRange TimeStamp;
        public long[] labels;
        public PriceRanges ranges;
        

    }
}
