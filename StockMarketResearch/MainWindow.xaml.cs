﻿using Newtonsoft.Json;
using StockMarketResearch.Windows.BackTesting;
using StockMarketResearch.Windows.Charting;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using StockMarketResearch.Strategies.IndicatorStrategies;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using StockMarketResearch.Windows.Earnings;
using StockMarketResearch.Windows.Ranking;
using StockMarketResearch.Network;
using StockMarketResearch.Windows.EarningsStrategy;
using System.Threading.Tasks;
using StockMarketResearch.GeneralCalculations;
using StockMarketResearch.Data.Network;
using StockMarketResearch.Indicators;
using StockMarketResearch.Windows.ObtainEarningDates;

namespace StockMarketResearch
{
    public partial class MainWindow : Window
    {
        int iTickerRanking = -1;
        int rankingGood = 0;
        Timer timerRanking;
        int timeInterval = 500;

        int iTickerIndexes = 0;
        int iTickerUK = 0;
        int iTickerUS = 0;
        Timer timerUK;
        Timer timerUS;
        Timer timerIndexes;
        YahooDataManager dataManager;
        
        public MainWindow()
        {
            InitializeComponent();
            dataManager = new YahooDataManager();
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
            progressBar.Maximum = dataManager.datasets[YahooDataManager.DataSetId.INDEXES].universe.tickers.Length +
                dataManager.datasets[YahooDataManager.DataSetId.UK].universe.tickers.Length + dataManager.datasets[YahooDataManager.DataSetId.US].universe.tickers.Length;
            timerIndexes = new System.Timers.Timer(timeInterval);
            timerIndexes.Elapsed += new ElapsedEventHandler(_timer_Elapsed_indexes);
            timerIndexes.Enabled = true;
            timerUK = new System.Timers.Timer(timeInterval);
            timerUK.Elapsed += new ElapsedEventHandler(_timer_Elapsed_ftse100);
            timerUK.Enabled = true;
            timerUS = new System.Timers.Timer(timeInterval);
            timerUS.Elapsed += new ElapsedEventHandler(_timer_Elapsed_sp500);
            timerUS.Enabled = true;

        }

        

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        void _timer_Elapsed_indexes(object sender, ElapsedEventArgs e)
        {
            timerAction(YahooDataManager.DataSetId.INDEXES, dataManager.datasets[YahooDataManager.DataSetId.INDEXES], ref iTickerIndexes, timerIndexes);
        }

        void _timer_Elapsed_ftse100(object sender, ElapsedEventArgs e)
        {
            timerAction(YahooDataManager.DataSetId.UK, dataManager.datasets[YahooDataManager.DataSetId.UK], ref iTickerUK, timerUK);
        }

        void _timer_Elapsed_sp500(object sender, ElapsedEventArgs e)
        {
            timerAction(YahooDataManager.DataSetId.US, dataManager.datasets[YahooDataManager.DataSetId.US], ref iTickerUS, timerUS);
        }


        void timerAction(YahooDataManager.DataSetId id, YahooDataSet dataset, ref int iTicker, System.Timers.Timer timer)
        {
            string t = dataset.universe.tickers[iTicker];
            iTicker++;
            //if (id == YahooDataManager.DataSetId.UK)
            //{
                download(id, t);
            //}

            if (iTicker < dataset.universe.tickers.Length)
            {

                Dispatcher.BeginInvoke(new Action(() =>
                {
                    progressBar.Value = iTickerIndexes + iTickerUK + iTickerUS;
                }));

            }
            else
            {
                timer.Enabled = false;
                if (id == YahooDataManager.DataSetId.US)
                {
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        MessageBoxResult result = MessageBox.Show("Finished!", "Download finished", MessageBoxButton.OK);
                        if(result == MessageBoxResult.OK)
                        {
                            progressBar.Value = 0;
                        }

                        /*ChartWindow w = new ChartWindow();
                        w.Show();
                        this.Close();*/
                    }));
                }
            }
        }

        private async void download(YahooDataManager.DataSetId id, string ticker)
        {
            try {
                //YahooStockResponse stockResponse = await DownloadUtil.downloadYahooFinanceData(id, ticker);
                YahooStockResponse stockResponse = await DownloadUtil.downloadGoogleIntraDayData(id, ticker);
                dataManager.updateDao(id, stockResponse);
            } catch(IOException exception)
            {
                System.Diagnostics.Debug.WriteLine(exception.Message + " " + ticker);
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine(exception.Message + " " + ticker);
            }
        }

        private void Calculate_Indicators_Button_Click(object sender, RoutedEventArgs e)
        {
            
            BackgroundWorker bgwCalculateIndicators = new BackgroundWorker();
            bgwCalculateIndicators.WorkerReportsProgress = true;
            progressBar.Maximum = 100;
            bgwCalculateIndicators.ProgressChanged += BgwCalculateIndicators_ProgressChanged;
            bgwCalculateIndicators.DoWork += BgwCalculateIndicators_DoWork;
            bgwCalculateIndicators.RunWorkerAsync();

        }

        private void BgwCalculateIndicators_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void BgwCalculateIndicators_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Start");
            BackgroundWorker worker = sender as BackgroundWorker;
            foreach (KeyValuePair<YahooDataManager.DataSetId, YahooDataSet> dataset in dataManager.datasets)
            {
                int i = 0;
                foreach (string ticker in dataset.Value.universe.tickers)
                {
                    i++;
                    worker.ReportProgress(100 * i / dataset.Value.universe.tickers.Length);
                    //System.Diagnostics.Debug.WriteLine(100 * i / dataset.Value.universe.tickers.Length);
                    IndicatorDao indicatorDao = CalculateIndicators.calculatIndicatorsForTicker(dataset.Value, ticker);
                    string json = JsonConvert.SerializeObject(indicatorDao, Formatting.Indented);
                    if (!File.Exists(dataset.Value.analysisFolder + dataset.Value.setFolder))
                    {
                        Directory.CreateDirectory(dataset.Value.analysisFolder + dataset.Value.setFolder);
                    }
                    File.WriteAllText(dataset.Value.analysisFolder + dataset.Value.setFolder + ticker + ".json", json);
                }
            }
        }

        private void Test_Strategy_Button_Click(object sender, RoutedEventArgs e)
        {
            MovingAverageStrategy movingAverageStrategy = new MovingAverageStrategy();
            IndicatorStrategyResultDao resultDao = movingAverageStrategy.calculateResult();

            string json = JsonConvert.SerializeObject(resultDao, Formatting.Indented);

            File.WriteAllText(dataManager.datasets[YahooDataManager.DataSetId.UK].analysisFolder + "//result.json", json);
            BackTestChartWindow win = new BackTestChartWindow();
            win.Show();
            this.Close();

        }

        private void Charts_Button_Click(object sender, RoutedEventArgs e)
        {
            ChartWindow w = new ChartWindow();
            w.Show();
            this.Close();
        }

        private void Cycles_Button_Click(object sender, RoutedEventArgs e)
        {
            DailyCycles w = new DailyCycles();
            w.calculate();
        }

        private void CompanyReports_Button_Click(object sender, RoutedEventArgs e)
        {
            EarningsWindow win = new EarningsWindow();
            win.Show();
            this.Close();
            
        }

        private void CompanyReportsChart_Button_Click(object sender, RoutedEventArgs e)
        {
            EarningsChartWindow win = new EarningsChartWindow();
            win.Show();
            this.Close();

        }

        private async void Update_Shares_Outstanding_Button_Click(object sender, RoutedEventArgs e)
        {
            List<TickerItem> tickerList = await DownloadUtil.DownloadTickerList();

            //Start downloading Shares outstanding for each ticker

            timerRanking = new Timer(600);
            timerRanking.Elapsed += (sender2, e2) => _timer_Elapsed_ranking(sender2, e2, tickerList);
            timerRanking.Enabled = true;
            
        }

        void _timer_Elapsed_ranking(object sender, ElapsedEventArgs e, List<TickerItem> tickerList)
        {
            if (iTickerRanking > tickerList.Count - 3)
            {
                timerRanking.Enabled = false;
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    MessageBox.Show("Update finished. " + rankingGood, "Finished!", MessageBoxButton.OK);
                    
                }));

            }
            iTickerRanking++;
            getSharesOutstanding(tickerList);
        }
             

        private async void getSharesOutstanding(List<TickerItem> tickerList)
        {
            var Response = await DownloadUtil.downloadYahooFinanceDaily(iTickerRanking, tickerList);

            Response = Response.Replace("\n", "");
            Response = Response.Replace("\"", "");
            string[] SplitResponse = Response.Split(',');
            string Ticker = SplitResponse[0];
            string SharesOutstanding = SplitResponse[1];
            if (!(SharesOutstanding.Equals("N/A")) && Convert.ToInt64(SharesOutstanding) > Math.Pow(10, 10) * 6)
            {
                System.Diagnostics.Debug.WriteLine("To high: " + Ticker + " " + SharesOutstanding);
                SharesOutstanding = (Convert.ToInt64(SharesOutstanding) / 1000).ToString();
            }
            
            if (!tickerList[iTickerRanking].IssuedShares.Equals(SharesOutstanding))
            {
                System.Diagnostics.Debug.WriteLine("Updating " + Ticker + " " + SharesOutstanding);
                updateSharesOutstanding(Ticker, SharesOutstanding);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(Ticker + " " + SharesOutstanding + " is up to date.");
            }            
        }

        private void updateSharesOutstanding(string ticker, string sharesOutstanding)
        {
            try
            {

                var soResponse = DownloadUtil.UpdateSharesOutstanding(ticker, sharesOutstanding);

                System.Diagnostics.Debug.WriteLine("update response: " + soResponse.Result);
                rankingGood++;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception: " + e.Data);
                //throw;
            }
            
        }

        private void Ranking_Button_Click(object sender, RoutedEventArgs e)
        {
            RankingWindow win = new RankingWindow();
            win.Show();
            this.Close();

        }

        private void Earnings_Strategy_Button_Click(object sender, RoutedEventArgs e)
        {
            EarningsStrategyWindow win = new EarningsStrategyWindow();
            win.Show();
            this.Close();
        }

        private async void Update_Earning_Dates_Button_Click(object sender, RoutedEventArgs e)
        {
            ObtainEarningDatesWindow win = await ObtainEarningDatesWindow.CreateAsync();
            win.Show();
            this.Close();
        }
        
    }
}
