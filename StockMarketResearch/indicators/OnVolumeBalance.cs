﻿using System;
using System.Collections.Generic;
using StockMarketResearch.Data.Base;

namespace StockMarketResearch.Indicators
{
    class OnVolumeBalance : IIndicator
    {
        public ICollection<T> calculateIndicator<T>(List<StockPriceSeriesItem> input, int period)
        {
            List<long> result = new List<long>();
            long runningTotal = 0;
            result.Add(0);
            for (int i = 1; i < input.Count; i++)
            {
                runningTotal += input[i].volume * Math.Sign(input[i].close - input[i - 1].close);
                result.Add(runningTotal);
            }

            return (ICollection<T>)result;
        }
    }
}
