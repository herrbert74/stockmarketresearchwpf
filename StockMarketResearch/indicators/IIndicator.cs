﻿using StockMarketResearch.Data.Base;
using System.Collections.Generic;

namespace StockMarketResearch.Indicators
{
    interface IIndicator
    {
        ICollection<T> calculateIndicator<T>(List<StockPriceSeriesItem> input, int period);
    }
}
