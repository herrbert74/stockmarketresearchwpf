﻿using Newtonsoft.Json;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System.Collections.Generic;
using System.IO;

namespace StockMarketResearch.Indicators
{
    class CalculateIndicators
    {
        public static IndicatorDao calculatIndicatorsForTicker(YahooDataSet dataset, string ticker)
        {
            //dtoJson = Regex.Replace(dtoJson, "\\)$", "");
            string stockDaoJson = File.ReadAllText(dataset.pricesFolder + dataset.setFolder + ticker + ".json");
            
            StockDao stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
            MovingAverage ma = new MovingAverage();
            List<double> ma200 = (List<double>)ma.calculateIndicator<double>(stockDao.series, 200);
            List<double> ma50 = (List<double>)ma.calculateIndicator<double>(stockDao.series, 50);
            List<double> ma20 = (List<double>)ma.calculateIndicator<double>(stockDao.series, 20);
            OnVolumeBalance onVolumeBalance = new OnVolumeBalance();
            List<long> ovb = (List<long>)onVolumeBalance.calculateIndicator<long>(stockDao.series, 0);
            MoneyFlowIndex moneyFlowIndex = new MoneyFlowIndex();
            List<double> mfi = (List<double>) moneyFlowIndex.calculateIndicator<double>(stockDao.series, 50);
            IndicatorDao indicatorDao = new IndicatorDao();
            indicatorDao.ticker = ticker;
            indicatorDao.ma200 = ma200;
            indicatorDao.ma50 = ma50;
            indicatorDao.ma20 = ma20;
            indicatorDao.OnVolumeBalance = ovb;
            indicatorDao.MoneyFlowIndex = mfi;
            return indicatorDao;
        }
    }
}
