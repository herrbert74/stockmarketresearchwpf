﻿using System;
using System.Collections.Generic;
using StockMarketResearch.Data.Base;

namespace StockMarketResearch.Indicators
{
    class MoneyFlowIndex : IIndicator
    {
        public ICollection<T> calculateIndicator<T>(List<StockPriceSeriesItem> input, int period)
        {
            List<double> result = new List<double>();
            Queue<double> typicalPrice = new Queue<double>();
            Queue<double> rawMoneyFlow = new Queue<double>();
            double positiveMoneyFlow = 0;
            double negativeMoneyFlow = 0;
            for (int i = 0; i < input.Count; i++)
            {
                typicalPrice.Enqueue((input[i].high + input[i].low + input[i].close) / 3);
                if (i > 0)
                {
                    double previousPrice = typicalPrice.Dequeue();

                    double rawMoneyFlowEntry = (Math.Sign(typicalPrice.Peek() - previousPrice) * typicalPrice.Peek()) * input[i].volume;
                    if (rawMoneyFlowEntry > 0)
                    {
                        positiveMoneyFlow += rawMoneyFlowEntry;
                    }
                    else
                    {
                        // negative * negative = positive
                        negativeMoneyFlow -= rawMoneyFlowEntry;
                    }
                    rawMoneyFlow.Enqueue(rawMoneyFlowEntry);
                }
                if (i < period)
                {
                    result.Add(50);
                }
                else
                {
                    if (rawMoneyFlow.Peek() > 0)
                    {
                        positiveMoneyFlow -= rawMoneyFlow.Peek();
                    }
                    else
                    {
                        negativeMoneyFlow += rawMoneyFlow.Peek();
                    }
                    double moneyFlowRatio = positiveMoneyFlow / negativeMoneyFlow;
                    result.Add(100 - 100 / (1 + moneyFlowRatio));
                    rawMoneyFlow.Dequeue();
                }
            }

            return (ICollection<T>)result;
        }
    }
}
