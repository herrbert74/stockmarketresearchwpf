﻿using StockMarketResearch.Data.Base;
using System.Collections.Generic;

namespace StockMarketResearch.Indicators
{
    class MovingAverage : IIndicator
    {
        public ICollection<T> calculateIndicator<T>(List<StockPriceSeriesItem> input, int period)
        {
            List<double> result = new List<double>();
            double runningTotal = 0;
            for(int i = 0; i < input.Count; i++)
            {
                runningTotal += input[i].close - ((i > period - 1) ? input[i-period].close : 0);
                result.Add(runningTotal / ((i > period - 2) ? period : (i + 1)));
            }
            
            return (ICollection<T>) result;
        }

        
    }
}
