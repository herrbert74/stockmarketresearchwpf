﻿using System.Windows;
using System.Windows.Controls;
using StockMarketResearch.Data.Yahoo;
using System.Collections.Generic;
using StockMarketResearch.Network;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Windows.Earnings
{
    public partial class EarningsChartWindow : Window
    {
        EarningsChartViewModel viewModel;
        
        int selectedTicker = 0;
        public YahooDataManager.DataSetId selectedDataSet = YahooDataManager.DataSetId.UK;
        List<EarningsDateItem> earningsDates;

        public EarningsChartWindow()
        {
            InitializeComponent();
            viewModel = (EarningsChartViewModel) this.DataContext;
            SelectedTicker.Text = (selectedTicker + 1).ToString();
            downloadDates();
        }

        private async void downloadDates()
        {
            earningsDates = await DownloadUtil.DownloadEarningsDates();
            DisplayTicker(0);
            FillComboBox(earningsDates);
        }

        private void FillComboBox(List<EarningsDateItem> stockResponse)
        {
            foreach (EarningsDateItem resultdateObject in stockResponse) {
                cmb2.Items.Add(resultdateObject.Ticker);
            }
        }

        private void Button_Previous_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker > 0)
            {
                DisplayTicker(--selectedTicker);
            }
        }

        private void Button_Next_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker < earningsDates.Count - 1)
            {
                DisplayTicker(++selectedTicker);
            }
        }

        private void Button_Previous_10_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker > 9)
            {
                selectedTicker -= 10;
                DisplayTicker(selectedTicker);
            }
        }

        private void Button_Next_10_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker < earningsDates.Count - 10)
            {
                selectedTicker += 10;
                DisplayTicker(selectedTicker);
            }
        }

        private void ComboBox_Selected(object sender, RoutedEventArgs e)
        {
            ComboBoxItem typeItem = (ComboBoxItem)cmb.SelectedItem;
            if (typeItem.Content != null)
            {
                string selectedText = typeItem.Content.ToString();
                if (selectedText.Equals("UK"))
                {                    
                    selectedDataSet = YahooDataManager.DataSetId.UK;
                    selectedTicker = 0;
                }
                else
                {
                    selectedDataSet = YahooDataManager.DataSetId.US;
                    selectedTicker = 0;
                }
                DisplayTicker(selectedTicker);
            }
        }

        private void ComboBox2_Selected(object sender, RoutedEventArgs e)
        {
            selectedTicker = cmb2.SelectedIndex;
            DisplayTicker(selectedTicker);
        }

        private void DisplayTicker(int selectedTicker)
        {
            cmb2.SelectedIndex = selectedTicker;
            viewModel.DisplayTicker(earningsDates[selectedTicker].ResultDate, earningsDates[selectedTicker].Ticker, earningsDates[selectedTicker].Country);

            SelectedTicker.Text = (selectedTicker + 1).ToString();

            plot.InvalidatePlot(true);
        }

        private void Button_Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow w = new MainWindow();
            w.Show();
            this.Close();
        }
    }
}
