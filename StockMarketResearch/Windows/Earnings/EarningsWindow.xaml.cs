﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StockMarketResearch.Windows.Earnings
{
    public partial class EarningsWindow : Window
    {
        EarningsViewModel btcvm;

        public EarningsWindow()
        {
            InitializeComponent();
            btcvm = (EarningsViewModel)this.DataContext;
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            btcvm.Previous();
            plot.InvalidatePlot();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            btcvm.Next();
            plot.InvalidatePlot();
        }
    }
}
