﻿using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Refit;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System;
using System.IO;
using System.Linq;
using StockMarketResearch.Data.Network;
using StockMarketResearch.Utils;
using StockMarketResearch.Network;

namespace StockMarketResearch.Windows.Earnings
{
    public class EarningsChartViewModel
    {

        public string Title { get; private set; }

        YahooDataManager dataManager = new YahooDataManager();

        public PlotModel plotModel { get; private set; }

        public StockDao stockDao;
        public IndicatorDao indicatorDao;

        public EarningsChartViewModel()
        {
            var xAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                LabelFormatter = _formatter,
                IntervalLength = 75,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                ExtraGridlineColor = OxyColor.FromArgb(255, 240, 185, 22),
                ExtraGridlineThickness = 2,
                Key = "xAxis",
            };

            var yPriceAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                Key = "priceAxis",
            };

            plotModel = new PlotModel();
            plotModel.Axes.Add(xAxis);
            plotModel.Axes.Add(yPriceAxis);

        }

        private string _formatter(double item)
        {
            if (stockDao != null && item > 0 && item < stockDao.series.Count)
            {
                DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                jTime = jTime.AddSeconds(stockDao.series[(int)item].Timestamp);
                return jTime.ToShortDateString() + "\n" + jTime.ToShortTimeString();
            }
            else
            {
                return "";
            }
        }

        public void DisplayTicker(string resultDate, string ticker, YahooDataManager.DataSetId id)
        {
           
            //Reset
            plotModel.Series.Clear();
            plotModel.Axes[0].Reset();
            plotModel.Axes[0].ExtraGridlines = new double[] { };
            plotModel.Axes[1].Reset();
            Boolean isLimitLineShown = false;

            LineSeries priceSeries = new LineSeries();
            LineSeries ma200Series = new LineSeries();
            //Get dataset and ticker
            YahooDataSet dataset = dataManager.datasets[id];
            //Get the data for the dataset and ticker
            string stockDaoJson = File.ReadAllText(dataset.pricesFolder + dataset.setFolder + ticker + ".json");
            stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
            string indicatorDaoJson = File.ReadAllText(dataset.analysisFolder + dataset.setFolder + ticker + ".json");
            indicatorDao = JsonConvert.DeserializeObject<IndicatorDao>(indicatorDaoJson);

            foreach (var item in stockDao.series.Select((x, i) => new { x, i }))
            {
                //Format the dates axis
                priceSeries.Points.Add(new DataPoint(item.i, item.x.close));
                var value = DateTime.Parse(resultDate, System.Globalization.CultureInfo.GetCultureInfo("en-us"));
                if (!isLimitLineShown && DateUtil.ConvertToTimestamp(value) < item.x.Timestamp)
                {
                    isLimitLineShown = true;
                    plotModel.Axes[0].ExtraGridlines = new double[] { item.i - 1 };

                }
            }

            foreach (var item in indicatorDao.ma200.Select((x, i) => new { x, i }))
            {
                ma200Series.Points.Add(new DataPoint(item.i, item.x));
            }

            plotModel.Title = stockDao.companyName;
            plotModel.Series.Add(priceSeries);
            //plotModel.Series.Add(ma200Series);
            downloadDailyData(ticker);
        }

        private async void downloadDailyData(string ticker)
        {
            LineSeries dailySeries = new LineSeries();
            try
            {
                GoogleStockPriceSeriesItem[] resp = await DownloadUtil.DownloadGoogleFinanceDaily(ticker);
                for (int i = 0; i < stockDao.series.Count; i++)
                {
                    DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);

                    // Add the number of seconds in UNIX timestamp to be converted.
                    dateTime = dateTime.AddSeconds(stockDao.series[i].Timestamp);

                    // The dateTime now contains the right date/time so to format the string,
                    // use the standard formatting methods of the DateTime object.
                    string dateToLookup = dateTime.ToString("d-MMM-yy");
                    for (int i2 = 0; i2 < resp.Length; i2++)
                    {
                        if (dateToLookup.Equals(resp[i2].Date))
                        {
                            dailySeries.Points.Add(new DataPoint(i, resp[i2].close));
                            break;
                        }
                        if (i2 == resp.Length)
                        {
                            dailySeries.Points.Add(new DataPoint(i, 0));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString() + ticker + "did not work");
                //throw;
            }
            plotModel.Series.Add(dailySeries);
            plotModel.InvalidatePlot(true);

        }

        

        public int getDataSetSize(YahooDataManager.DataSetId selectedDataSet)
        {
            return dataManager.datasets[selectedDataSet].universe.tickers.Length;
        }
    }
}
