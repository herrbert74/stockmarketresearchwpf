﻿using Newtonsoft.Json;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Input;
using StockMarketResearch.Indicators;

namespace StockMarketResearch.Windows.Analysis
{
    public partial class AnalysisWindow : Window
    {
        YahooDataManager dataManager = new YahooDataManager();

        public AnalysisWindow()
        {
            InitializeComponent();
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            foreach (KeyValuePair<YahooDataManager.DataSetId, YahooDataSet> dataset in dataManager.datasets)
            {
                progressBar.Maximum = dataset.Value.universe.tickers.Length;
                progressBar.Value = 0;
                int i = 0;
                foreach (string ticker in dataset.Value.universe.tickers)
                {
                    i++;
                    progressBar.Value = i;
                    string stockDaoJson = File.ReadAllText(dataset.Value.pricesFolder + dataset.Value.setFolder + ticker + ".json");
                    StockDao stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
                    MovingAverage ma = new MovingAverage();
                    List<double> ma200 = (List<double>)ma.calculateIndicator<double>(stockDao.series, 200);
                    List<double> ma50 = (List<double>)ma.calculateIndicator<double>(stockDao.series, 50);
                    List<double> ma20 = (List<double>)ma.calculateIndicator<double>(stockDao.series, 20);
                    IndicatorDao indicatorDao = new IndicatorDao();
                    indicatorDao.ticker = ticker;
                    indicatorDao.ma200 = ma200;
                    indicatorDao.ma50 = ma50;
                    indicatorDao.ma20 = ma20;
                    string json = JsonConvert.SerializeObject(indicatorDao, Formatting.Indented);
                    if (!File.Exists(dataset.Value.analysisFolder + dataset.Value.setFolder))
                    {
                        Directory.CreateDirectory(dataset.Value.analysisFolder + dataset.Value.setFolder);
                    }
                    File.WriteAllText(dataset.Value.analysisFolder + dataset.Value.setFolder + ticker + ".json", json);
                }
                             

            }
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }
}
