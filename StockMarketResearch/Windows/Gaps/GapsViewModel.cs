﻿using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Series;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StockMarketResearch.Windows.Gaps
{
    public class GapsViewModel
    {

        public string Title { get; private set; }

        int iResult = 0;

        public PlotModel plotModel { get; private set; }

        SortedDictionary<double, GAP_CATEGORY> gapMap = new SortedDictionary<double, GAP_CATEGORY>();
        
        Dictionary<StartValues, ResultValues> triggeredCases = new Dictionary<StartValues, ResultValues>();
        Dictionary<GAP_CATEGORY, List<double>> categorizedCases = new Dictionary<GAP_CATEGORY, List<double>>();

        Dictionary<double, int> bucketeer = new Dictionary<double, int>();

        LineSeries overlayData = new LineSeries();
        RectangleBarSeries columnSeries = new RectangleBarSeries();

        YahooDataManager dataManager = new YahooDataManager();

        public GapsViewModel()
        {
            plotModel = new PlotModel();

            createMaps();
            calculateTriggeredCases();
            getCategorizedCases();

            getChartData();

        }

        public void createMaps()
        {
            gapMap.Add(1d, GAP_CATEGORY.GAP_1_2);
            gapMap.Add(2d, GAP_CATEGORY.GAP_M3);
            gapMap.Add(3d, GAP_CATEGORY.GAP_M4);
            gapMap.Add(4d, GAP_CATEGORY.GAP_M5);
            gapMap.Add(5d, GAP_CATEGORY.GAP_M6);
            gapMap.Add(6d, GAP_CATEGORY.GAP_M7);
            gapMap.Add(7d, GAP_CATEGORY.GAP_7PLUS);
        }

        public void calculateTriggeredCases()
        {
            foreach (KeyValuePair<YahooDataManager.DataSetId, YahooDataSet> dataset in dataManager.datasets)
            {
                if (dataset.Key != YahooDataManager.DataSetId.INDEXES)
                {
                    foreach (string ticker in dataset.Value.universe.tickers)
                    {
                        addTriggeredCases(dataset.Value, ticker);
                    }
                }
            }
        }



        private void addTriggeredCases(YahooDataSet dataset, string ticker)
        {
            string stockDaoJson = File.ReadAllText(dataset.pricesFolder + ticker + ".json");
            StockDao stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
            STATE state = STATE.NORMAL;
            StartValues startValues = new StartValues(ticker);
            ResultValues resultValues = new ResultValues();
            double previousClose = stockDao.series[0].close;
            double gapIndex = 0;
            foreach (var seriesItem in stockDao.series.Select((value, i) => new { value, i }))
            {
                switch (state)
                {
                    case (STATE.NORMAL):
                        if (stockDao.series[seriesItem.i].close > previousClose * 1.01)
                        {
                            state = STATE.GAP;
                            gapIndex = seriesItem.i;
                            startValues.openTime = stockDao.series[seriesItem.i].Timestamp;
                            startValues.openDifference = stockDao.series[seriesItem.i].close / previousClose;
                        }
                        else
                        {
                            previousClose = stockDao.series[seriesItem.i].close;
                        }
                        break;
                    case (STATE.GAP):
                        if (seriesItem.i > gapIndex + 6)
                        {
                            state = STATE.HALF_HOUR_PASSED;
                            startValues.halfHourTimeStamp = stockDao.series[seriesItem.i].Timestamp;
                            startValues.halfHourDifference = stockDao.series[seriesItem.i].close / previousClose;
                            startValues.halfHourClose = stockDao.series[seriesItem.i].close;
                        }
                        break;
                    case (STATE.HALF_HOUR_PASSED):
                        if (seriesItem.i > gapIndex + 101)
                        {
                            state = STATE.NORMAL;
                            resultValues.endOfFirstDayTime = stockDao.series[seriesItem.i].Timestamp;
                            resultValues.endOfFirstDayDifference = stockDao.series[seriesItem.i].close / startValues.halfHourClose;
                            triggeredCases.Add(startValues, resultValues);
                            startValues = new StartValues(ticker);
                            resultValues = new ResultValues();
                        }
                        break;
                    
                }
            }
        }

        private void getCategorizedCases()
        {
            //Create empty maps with arraylists
            foreach (GAP_CATEGORY gapCategory in Enum.GetValues(typeof(GAP_CATEGORY)))
            {
                    List<double> list = new List<double>();
                    categorizedCases.Add(gapCategory, list);
            }

            List<double> d = new List<double>();
        
            //Convert values
            foreach (KeyValuePair<StartValues, ResultValues> entry in triggeredCases)
            {
                double openDifferencePercentage = (entry.Key.openDifference - 1) * 100d;
                double halfHourDifferencePercentage = (entry.Key.halfHourDifference - 1) * 100d;
                GAP_CATEGORY gapCategory = gapMap.FloorEntry(openDifferencePercentage).Value;
                categorizedCases[gapCategory].Add(halfHourDifferencePercentage);
            }
        }

        private void getChartData()
        {
            double[] values;
            plotModel.Series.Clear();
            overlayData = new LineSeries();
            columnSeries = new RectangleBarSeries();
            
            GAP_CATEGORY gapCategory = (GAP_CATEGORY)Enum.ToObject(typeof(GAP_CATEGORY), iResult);
            plotModel.Title = gapCategory.ToString();
            values = categorizedCases[gapCategory].ToArray();

            const int StepsNumber = 30;
            // Choosing the size of each bucket
            if (values.Count() > 1)
            {
                double step = (values.Max() - values.Min()) / StepsNumber;

                double mean = values.Average();
                double deviationSq = values.Select(x => Math.Pow(x - mean, 2)).Average();

                bucketeer = new Dictionary<double, int>();
                for (double curr = values.Min(); curr <= values.Max(); curr += step)
                {
                    // Counting the values that can be put in the bucket and dividing them on values.Count()
                    var count = values.Where(x => x >= curr && x < curr + step).Count();
                    bucketeer.Add(curr, count);
                }

                // Then I build normal distribution overlay 
                double x0 = values.Min();
                double x1 = values.Max();
                for (int i = 0; i < StepsNumber; i++)
                {
                    double x = x0 + (x1 - x0) * i / (StepsNumber - 1);
                    double f = 1.0 / Math.Sqrt(2 * Math.PI * deviationSq) * Math.Exp(-(x - mean) * (x - mean) / 2 / deviationSq) * values.Count();
                    overlayData.Points.Add(new DataPoint(x, f));
                }
               
                foreach (var pair in bucketeer.OrderBy(x => x.Key))
                {
                    columnSeries.Items.Add(new RectangleBarItem(pair.Key, 0, pair.Key + step, pair.Value));
                }
                plotModel.Series.Add(columnSeries);
                plotModel.Series.Add(overlayData);
            }
        }

        public void Next()
        {
            iResult++;
            getChartData();
        }

        public void Previous()
        {
            iResult--;
            getChartData();
        }
    }
}
