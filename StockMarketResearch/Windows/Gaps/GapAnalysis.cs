﻿namespace StockMarketResearch.Windows.Gaps
{
    public class StartValues
    {
        public StartValues(string ticker)
        {
            this.ticker = ticker;
        }

        public string ticker;
        public long openTime;
        public double openDifference;
        public long halfHourTimeStamp;
        public double halfHourClose;
        public double halfHourDifference;
    }

   

    class ResultValues
    {
        public long endOfFirstDayTime;
        public double endOfFirstDayDifference;
        
    }

    enum STATE { NORMAL, GAP, HALF_HOUR_PASSED, ONE_DAY_PASSED, TWO_DAYS_PASSED };
    public enum GAP_CATEGORY { GAP_1_2, GAP_M3, GAP_M4, GAP_M5, GAP_M6, GAP_M7, GAP_7PLUS };
   

}
