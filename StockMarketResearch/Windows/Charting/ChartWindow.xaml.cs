﻿using System.Windows;
using System.Windows.Controls;
using StockMarketResearch.Data.Yahoo;

namespace StockMarketResearch.Windows.Charting
{
    public partial class ChartWindow : Window
    {
        ChartViewModel viewModel;
        
        int selectedTicker = 0;
        YahooDataManager.DataSetId selectedDataSet = YahooDataManager.DataSetId.UK;

        public ChartWindow()
        {
            InitializeComponent();
            viewModel = (ChartViewModel) this.DataContext;
            SelectedTicker.Text = (selectedTicker + 1).ToString();
            FillComboBox2(viewModel);
            cmb2.SelectedIndex = selectedTicker;
        }

        private void FillComboBox2(ChartViewModel viewModel)
        {
            foreach (string ticker in viewModel.getDataSetTickers(selectedDataSet))
            {
                cmb2.Items.Add(ticker);
            }
        }

        private void Button_Previous_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker > 0)
            {
                DisplayTicker(--selectedTicker);
            }
        }

        private void Button_Next_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker < viewModel.getDataSetSize(selectedDataSet) - 1)
            {
                DisplayTicker(++selectedTicker);
            }
        }

        private void Button_Previous_10_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker > 9)
            {
                selectedTicker -= 10;
                DisplayTicker(selectedTicker);
            }
        }

        private void Button_Next_10_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker < viewModel.getDataSetSize(selectedDataSet) - 10)
            {
                selectedTicker += 10;
                DisplayTicker(selectedTicker);
            }
        }


        private void ComboBox_Selected(object sender, RoutedEventArgs e)
        {
            ComboBoxItem typeItem = (ComboBoxItem)cmb.SelectedItem;
            if (typeItem.Content != null)
            {
                string selectedText = typeItem.Content.ToString();
                if (selectedText.Equals("UK"))
                {                    
                    selectedDataSet = YahooDataManager.DataSetId.UK;
                    selectedTicker = 0;
                }
                else
                {
                    selectedDataSet = YahooDataManager.DataSetId.US;
                    selectedTicker = 0;
                }
                DisplayTicker(selectedTicker);
            }
        }

        private void ComboBox2_Selected(object sender, RoutedEventArgs e)
        {
            selectedTicker = cmb2.SelectedIndex;
            DisplayTicker(selectedTicker);
        }

        private void DisplayTicker(int selectedTicker)
        {
            cmb2.SelectedIndex = selectedTicker;

            viewModel.DisplayTicker(selectedDataSet, selectedTicker);

            SelectedTicker.Text = (selectedTicker + 1).ToString();

            plot.InvalidatePlot(true);
        }

        private void Button_Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow w = new MainWindow();
            w.Show();
            this.Close();
        }
    }
}
