﻿using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using StockMarketResearch.Data.Base;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System;
using System.IO;
using System.Linq;

namespace StockMarketResearch.Windows.Charting
{
    public class ChartViewModel
    {

        public string Title { get; private set; }

        YahooDataManager dataManager = new YahooDataManager();

        public PlotModel plotModel { get; private set; }

        public StockDao stockDao;

        public ChartViewModel()
        {
            var xAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                //StringFormat = "dd-MM-yy HH:mm",
                LabelFormatter = _formatter,
                IntervalLength = 75,
                //MinorIntervalType = DateTimeIntervalType.Hours,
                //IntervalType = DateTimeIntervalType.Days,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
            };

            var yPriceAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                Key = "priceAxis",
            };

            plotModel = new PlotModel();
            plotModel.Axes.Add(xAxis);
            plotModel.Axes.Add(yPriceAxis);

            DisplayTicker(YahooDataManager.DataSetId.UK, 0);
        }

        private string _formatter(double item)
        {
            if (item > 0 && item < stockDao.series.Count)
            {
                DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                jTime = jTime.AddSeconds(stockDao.series[(int)item].Timestamp);
                return jTime.ToShortDateString();
            }
            else
            {
                return "";
            }
        }

        public void DisplayTicker(YahooDataManager.DataSetId id, int selectedItem)
        {
            //Reset
            plotModel.Series.Clear();
            plotModel.Axes[0].Reset();
            plotModel.Axes[1].Reset();
            LineSeries priceSeries = new LineSeries();
            //Get dataset and ticker
            YahooDataSet dataset = dataManager.datasets[id];
            string ticker = dataset.universe.tickers[selectedItem];
            //Get the data for the dataset and ticker
            string stockDaoJson = File.ReadAllText(dataset.pricesFolder + dataset.setFolder + ticker + ".json");
            stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
            
            foreach (var item in stockDao.series.Select((x, i) => new {x, i }))
            {
                //Format the dates axis
                DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                jTime = jTime.AddSeconds(item.x.Timestamp);
                //jTime.ToShortDateString();
                priceSeries.Points.Add(new DataPoint(item.i, item.x.close));
            }

            plotModel.Title = stockDao.companyName;
            plotModel.Series.Add(priceSeries);
        }

        public int getDataSetSize(YahooDataManager.DataSetId selectedDataSet)
        {
            return dataManager.datasets[selectedDataSet].universe.tickers.Length;
        }

        public string[] getDataSetTickers(YahooDataManager.DataSetId selectedDataSet)
        {
            return dataManager.datasets[selectedDataSet].universe.tickers;
        }
    }
}
