﻿using System;

namespace StockMarketResearch.Windows.GapsStrategy
{
    public class StartValues
    {
        public StartValues(string ticker)
        {
            this.ticker = ticker;
        }

        public string ticker;
        public long openTime;
        public double openDifference;
        public long halfHourTimeStamp;
        public double halfHourClose;
        public double halfHourDifference;
    }

    public class StartCategories:IEquatable<StartCategories>
    {
        public override int GetHashCode()
        {
            return this.gapCategory.GetHashCode() + this.firstHalfHourCategory.GetHashCode();
        }

        public StartCategories(GAP_CATEGORY gapCategory, FIRST_HALF_HOUR_CATEGORY firstHalfHourCategory)
        {
            this.gapCategory = gapCategory;
            this.firstHalfHourCategory = firstHalfHourCategory;
        }
        public GAP_CATEGORY gapCategory;
        public FIRST_HALF_HOUR_CATEGORY firstHalfHourCategory;
                
        public bool Equals(StartCategories other)
        {
            return gapCategory.Equals(other.gapCategory) && firstHalfHourCategory.Equals(other.firstHalfHourCategory);
        }
    }

    class ResultValues
    {
        public long endOfFirstDayTime;
        public long endOfSecondDayTime;
        public long endOfFirstWeekTime;
        public double endOfFirstDayDifference;
        public double endOfSecondDayDifference;
        public double endOfFirstWeekDifference;
    }

    enum STATE { NORMAL, GAP, HALF_HOUR_PASSED, ONE_DAY_PASSED, TWO_DAYS_PASSED };
    public enum GAP_CATEGORY { GAP_1_3, GAP_3_6, GAP_6_PLUS };
    public enum FIRST_HALF_HOUR_CATEGORY { FHH_TO_MINUS_8, FHH_MINUS_8_4, FHH_MINUS_4_1, FHH_MINUS_1_PLUS_1, FHH_1_TO_4, FHH_4_TO__8, FHH_8_PLUS };

}
