﻿using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Series;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StockMarketResearch.Windows.GapsStrategy
{ 
    public class GapAnalysisChartViewModel
    {

        public string Title { get; private set; }

        int iResult = 0;

        public PlotModel plotModel { get; private set; }

        SortedDictionary<double, GAP_CATEGORY> gapMap = new SortedDictionary<double, GAP_CATEGORY>();
        SortedDictionary<double, FIRST_HALF_HOUR_CATEGORY> firstHourMap = new SortedDictionary<double, FIRST_HALF_HOUR_CATEGORY>();

        Dictionary<StartValues, ResultValues> triggeredCases = new Dictionary<StartValues, ResultValues>();
        Dictionary<StartCategories, List<double>> categorizedCases = new Dictionary<StartCategories, List<double>>();

        Dictionary<double, int> bucketeer = new Dictionary<double, int>();

        LineSeries overlayData = new LineSeries();
        RectangleBarSeries columnSeries = new RectangleBarSeries();

        YahooDataManager manager = new YahooDataManager();

        public GapAnalysisChartViewModel()
        {
            plotModel = new PlotModel();

            createMaps();
            calculateTriggeredCases();
            getCategorizedCases();

            getChartData();

            //TODO: CONTINUE
            //http://stackoverflow.com/questions/27134057/draw-histogram-with-normal-distribution-overlay-from-data
            //int z = 1;
        }

        public void createMaps()
        {
            gapMap.Add(-8d, GAP_CATEGORY.GAP_1_3);
            gapMap.Add(3d, GAP_CATEGORY.GAP_3_6);
            gapMap.Add(6d, GAP_CATEGORY.GAP_6_PLUS);

            firstHourMap.Add(-1000d, FIRST_HALF_HOUR_CATEGORY.FHH_TO_MINUS_8);
            firstHourMap.Add(-8d, FIRST_HALF_HOUR_CATEGORY.FHH_MINUS_8_4);
            firstHourMap.Add(-4d, FIRST_HALF_HOUR_CATEGORY.FHH_MINUS_4_1);
            firstHourMap.Add(-1d, FIRST_HALF_HOUR_CATEGORY.FHH_MINUS_1_PLUS_1);
            firstHourMap.Add(1d, FIRST_HALF_HOUR_CATEGORY.FHH_1_TO_4);
            firstHourMap.Add(4d, FIRST_HALF_HOUR_CATEGORY.FHH_4_TO__8);
            firstHourMap.Add(8d, FIRST_HALF_HOUR_CATEGORY.FHH_8_PLUS);

            System.Diagnostics.Debug.WriteLine("see how useful this is? (looking up indices that aren't in my map)");
            System.Diagnostics.Debug.WriteLine(gapMap.FloorEntry(2.0f));
            System.Diagnostics.Debug.WriteLine(gapMap.CeilingEntry(2.0f));
        }

        public void calculateTriggeredCases()
        {
            foreach (KeyValuePair<YahooDataManager.DataSetId, YahooDataSet> dataset in manager.datasets)
            {
                if (dataset.Key != YahooDataManager.DataSetId.INDEXES)
                {
                    foreach (string ticker in dataset.Value.universe.tickers)
                    {
                        addTriggeredCases(dataset.Value, ticker);
                    }
                }
            }
        }

        private void addTriggeredCases(YahooDataSet dataset, string ticker)
        {
            string stockDaoJson = File.ReadAllText(dataset.pricesFolder + ticker + ".json");
            StockDao stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
            STATE state = STATE.NORMAL;
            StartValues startValues = new StartValues(ticker);
            ResultValues resultValues = new ResultValues();
            double previousClose = stockDao.series[0].close;
            double gapIndex = 0;
            foreach (var seriesItem in stockDao.series.Select((value, i) => new { value, i }))
            {
                switch (state)
                {
                    case (STATE.NORMAL):
                        if (stockDao.series[seriesItem.i].close > previousClose * 1.01)
                        {
                            state = STATE.GAP;
                            gapIndex = seriesItem.i;
                            startValues.openTime = stockDao.series[seriesItem.i].Timestamp;
                            startValues.openDifference = stockDao.series[seriesItem.i].close / previousClose;
                        }
                        else
                        {
                            previousClose = stockDao.series[seriesItem.i].close;
                        }
                        break;
                    case (STATE.GAP):
                        if (seriesItem.i > gapIndex + 6)
                        {
                            state = STATE.HALF_HOUR_PASSED;
                            startValues.halfHourTimeStamp = stockDao.series[seriesItem.i].Timestamp;
                            startValues.halfHourDifference = stockDao.series[seriesItem.i].close / previousClose;
                            startValues.halfHourClose = stockDao.series[seriesItem.i].close;
                        }
                        break;
                    case (STATE.HALF_HOUR_PASSED):
                        if (seriesItem.i > gapIndex + 101)
                        {
                            state = STATE.ONE_DAY_PASSED;
                            resultValues.endOfFirstDayTime = stockDao.series[seriesItem.i].Timestamp;
                            resultValues.endOfFirstDayDifference = stockDao.series[seriesItem.i].close / startValues.halfHourClose;

                        }
                        break;
                    case (STATE.ONE_DAY_PASSED):
                        if (seriesItem.i > gapIndex + 203)
                        {
                            state = STATE.TWO_DAYS_PASSED;
                            resultValues.endOfSecondDayTime = stockDao.series[seriesItem.i].Timestamp;
                            resultValues.endOfSecondDayDifference = stockDao.series[seriesItem.i].close / startValues.halfHourClose;
                        }
                        break;
                    case (STATE.TWO_DAYS_PASSED):
                        if (seriesItem.i > gapIndex + 713)
                        {
                            state = STATE.NORMAL;
                            resultValues.endOfFirstWeekTime = stockDao.series[seriesItem.i].Timestamp;
                            resultValues.endOfFirstWeekDifference = stockDao.series[seriesItem.i].close / startValues.halfHourClose;
                            triggeredCases.Add(startValues, resultValues);
                            startValues = new StartValues(ticker);
                            resultValues = new ResultValues();
                        }
                        break;
                }
            }
        }

        private void getCategorizedCases()
        {
            //Create empty maps with arraylists
            foreach (GAP_CATEGORY gapCategory in Enum.GetValues(typeof(GAP_CATEGORY)))
            {
                foreach (FIRST_HALF_HOUR_CATEGORY firstHalfHourCategory in Enum.GetValues(typeof(FIRST_HALF_HOUR_CATEGORY)))
                {
                    StartCategories cat = new StartCategories(gapCategory, firstHalfHourCategory);
                    List<double> list = new List<double>();
                    categorizedCases.Add(cat, list);
                }
            }
            List<double> d = new List<double>();
            //Convert values
            foreach (KeyValuePair<StartValues, ResultValues> entry in triggeredCases)
            {
                double openDifferencePercentage = (entry.Key.openDifference - 1) * 100d;
                double halfHourDifferencePercentage = (entry.Key.halfHourDifference - 1) * 100d;
                GAP_CATEGORY gapCategory = gapMap.FloorEntry(openDifferencePercentage).Value;
                FIRST_HALF_HOUR_CATEGORY firstHalfHourCategory = firstHourMap.FloorEntry(halfHourDifferencePercentage).Value;
                StartCategories cat = new StartCategories(gapCategory, firstHalfHourCategory);
                d = categorizedCases[cat];
                d.Add(entry.Value.endOfFirstWeekDifference);
                categorizedCases[cat] = d;
            }
            //Create empty maps with arraylists
            foreach (GAP_CATEGORY gapCategory in Enum.GetValues(typeof(GAP_CATEGORY)))
            {
                foreach (FIRST_HALF_HOUR_CATEGORY firstHalfHourCategory in Enum.GetValues(typeof(FIRST_HALF_HOUR_CATEGORY)))
                {
                    StartCategories cat = new StartCategories(gapCategory, firstHalfHourCategory);
                    System.Diagnostics.Debug.WriteLine(gapCategory.ToString() + " " + firstHalfHourCategory.ToString() + " " + categorizedCases[cat].Count);
                }
            }
        }

        private void getChartData()
        {
            double[] values;
            plotModel.Series.Clear();
            overlayData = new LineSeries();
            columnSeries = new RectangleBarSeries();
            
            GAP_CATEGORY gapCategory = (GAP_CATEGORY)Enum.ToObject(typeof(GAP_CATEGORY), iResult / 7);
            FIRST_HALF_HOUR_CATEGORY firstHalfHourCategory = (FIRST_HALF_HOUR_CATEGORY)Enum.ToObject(typeof(FIRST_HALF_HOUR_CATEGORY), iResult % 7);
            plotModel.Title = gapCategory.ToString() + " " + firstHalfHourCategory.ToString();
            StartCategories cat = new StartCategories(gapCategory, firstHalfHourCategory);
            values = categorizedCases[cat].ToArray();

            const int StepsNumber = 30;
            // Choosing the size of each bucket
            if (values.Count() > 1)
            {
                double step = (values.Max() - values.Min()) / StepsNumber;

                double mean = values.Average();
                double deviationSq = values.Select(x => Math.Pow(x - mean, 2)).Average();

                bucketeer = new Dictionary<double, int>();
                for (double curr = values.Min(); curr <= values.Max(); curr += step)
                {
                    // Counting the values that can be put in the bucket and dividing them on values.Count()
                    var count = values.Where(x => x >= curr && x < curr + step).Count();
                    bucketeer.Add(curr, count);
                }

                // Then I build normal distribution overlay 
                double x0 = values.Min();
                double x1 = values.Max();
                for (int i = 0; i < StepsNumber; i++)
                {
                    double x = x0 + (x1 - x0) * i / (StepsNumber - 1);
                    double f = 1.0 / Math.Sqrt(2 * Math.PI * deviationSq) * Math.Exp(-(x - mean) * (x - mean) / 2 / deviationSq);
                    overlayData.Points.Add(new DataPoint(x, f));
                }
               
                foreach (var pair in bucketeer.OrderBy(x => x.Key))
                {
                    columnSeries.Items.Add(new RectangleBarItem(pair.Key, 0, pair.Key + step, pair.Value));
                }
                plotModel.Series.Add(columnSeries);
                plotModel.Series.Add(overlayData);
            }
        }

        public void Next()
        {
            iResult++;
            getChartData();
        }

        public void Previous()
        {
            iResult--;
            getChartData();
        }
    }
}
