﻿using Newtonsoft.Json;
using Refit;
using StockMarketResearch.Utils;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using StockMarketResearch.Data.Network;
using System.Windows.Data;
using System.ComponentModel;
using System;
using StockMarketResearch.Network;

namespace StockMarketResearch.Windows.Ranking
{
    public partial class RankingWindow : Window
    {
        int iPricesDownloaded = -1;

        HashSet<int> ftse100_ticker_ids = new HashSet<int>();
        HashSet<int> ftse250_ticker_ids = new HashSet<int>();
        HashSet<int> ftse_all_share_ticker_ids = new HashSet<int>();
        HashSet<int> ftse_delisted_ticker_ids = new HashSet<int>();

        List<RankingTickerItem> rankingList = new List<RankingTickerItem>();

        public RankingWindow()
        {
            InitializeComponent();
            DownloadIndexChanges();
        }

        private async void DownloadIndexChanges()
        {
            var stockService = RestService.For<IIndexChangesService>("http://herrbert74.biz.ht");
            var response = await stockService.GetIndexChanges();

            var indexChanges = JsonConvert.DeserializeObject<List<IndexChangesResponseDto>>(response, new BooleanJsonConverter());

            HashSet<int> set;
            foreach (IndexChangesResponseDto change in indexChanges)
            {
                switch (change.index_id)
                {
                    case 0:
                        set = ftse_delisted_ticker_ids;
                        break;
                    case 1:
                        set = ftse100_ticker_ids;
                        break;
                    case 2:
                        set = ftse250_ticker_ids;
                        break;
                    case 3:
                        set = ftse_all_share_ticker_ids;
                        break;
                    default:
                        set = ftse_all_share_ticker_ids;
                        break;
                }

                //Do the change: add or remove
                if (change.addition)
                {
                    set.Add(change.ticker_id);

                }
                else
                {
                    set.Remove(change.ticker_id);
                }
            }
            List<TickerItem> tickerList = await DownloadUtil.DownloadTickerList();


            fillRankingListWithTickersData(tickerList);
            foreach (TickerItem item in tickerList)
            {
                    getPrices(item.Ticker);
            }

        }

        private async void getPrices(string ticker)
        {
            try
            {
                GoogleStockPriceSeriesItem[] priceItems = await DownloadUtil.DownloadGoogleFinanceDaily(ticker);
            

                double close;
                if (priceItems.Length > 0)
                {
                    close = priceItems[0].close;
                }
                else
                {
                    close = 0.01;
                }

                var dict = rankingList.ToDictionary(x => x.Ticker);
                RankingTickerItem found;
                if (dict.TryGetValue(ticker, out found))
                {

                    found.price = close;
                }

                iPricesDownloaded++;

                if (iPricesDownloaded == rankingList.Count - 1)
                {
                    var gridItems = new List<GridItem>();
                    foreach (RankingTickerItem item in rankingList)
                    {
                        if (item.IssuedShares.Equals("N/A"))
                        {
                            item.IssuedShares = "1";
                        }
                        item.marketCap = item.price * Convert.ToInt64(item.IssuedShares) / 100;

                        string Index = "";
                        switch (item.index) {
                            case 0:
                                Index = "FTSE DELISTED";
                                break;
                            case 1:
                                Index = "FTSE100";
                                break;
                            case 2:
                                Index = "FTSE250";
                                break;
                            case 3:
                                Index = "FTSE ALL SHARE";
                                break;
                            default:
                                Index = "FTSE ALL SHARE";
                                break;
                        }
                        gridItems.Add(new GridItem { Rank = item.id, Ticker = item.Ticker, Name = item.Name, Price = item.price, Index = Index, MarketCap = item.marketCap });
                    }
                    lvRanking.ItemsSource = gridItems;

                    var view = (CollectionView)CollectionViewSource.GetDefaultView(lvRanking.ItemsSource);
                    view.SortDescriptions.Add(new SortDescription("MarketCap", ListSortDirection.Descending));
                    int i = 1;
                    foreach(GridItem item in view)
                    {
                        item.Rank = i;
                        i++;
                    }
                    MessageBox.Show("ftse 100: " + ftse100_ticker_ids.Count + "\nftse 250: " + ftse250_ticker_ids.Count + "\nftse all share: " + ftse_all_share_ticker_ids.Count + "\nftse delisted: " + ftse_delisted_ticker_ids.Count, "Finished", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                iPricesDownloaded++;
                //throw;

            }
        }

        private void fillRankingListWithTickersData(List<TickerItem> stockResponse)
        {
            foreach (TickerItem item in stockResponse)
            {
                int index = 3; //default to ALL SHARE
                if (ftse100_ticker_ids.Contains(item.id))
                {
                    index = 1;
                }
                else if (ftse250_ticker_ids.Contains(item.id))
                {
                    index = 2;
                }
                else if (ftse_all_share_ticker_ids.Contains(item.id))
                {
                    index = 3;
                }
                else if (ftse_delisted_ticker_ids.Contains(item.id))
                {
                    index = 0;
                }
                var rankingItem = new RankingTickerItem(item.id, item.Ticker, item.Name, item.IssuedShares, 0, 0, index);
                rankingList.Add(rankingItem);
            }
        }
    }

    public class GridItem
    {
        public int Rank { get; set; }

        public string Ticker { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string Index { get; set; }

        public double MarketCap { get; set; }
    }
}
