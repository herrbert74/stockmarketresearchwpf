﻿using StockMarketResearch.Data.Network;
using StockMarketResearch.Network;
using StockMarketResearch.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StockMarketResearch.Windows.ObtainEarningDates
{
    //http://blog.stephencleary.com/2013/01/async-oop-2-constructors.html

    /// <summary>
    /// Interaction logic for GetEarningDatesWindow.xaml
    /// </summary>
    public partial class ObtainEarningDatesWindow : Window
    {
        private ObtainEarningDatesWindow()
        {
            InitializeComponent();
        }

        public static Task<ObtainEarningDatesWindow> CreateAsync()
        {
            var ret = new ObtainEarningDatesWindow();
            return ret.InitializeAsync();
        }

        public async Task<ObtainEarningDatesWindow> InitializeAsync()
        {
            var tickerItems = await GetTickerList();
            var res = await ObtainEarningDates(tickerItems);
            var existingEarningDates = await DownloadUtil.DownloadEarningsDates();
            foreach(EarningsDateItem item in res)
            {
                if(!existingEarningDates.Any(p => p.ResultDate == item.ResultDate)){
                    var result = await DownloadUtil.UploadEarningsDate(item.ResultDate, item.Ticker, item.Country.ToString(), item.Description);
                    System.Diagnostics.Debug.WriteLine(result);
                }
            }
            return this;

        }

        async Task<List<TickerItem>> GetTickerList()
        {
            return await DownloadUtil.DownloadTickerList();
        }

        static async Task<List<EarningsDateItem>> ObtainEarningDates(List<TickerItem> tickerItems)
        {
            List<EarningsDateItem> obtainedEarningsDateItems = new List<EarningsDateItem>();
            string pattern = "large90 left\">(\\d\\d/\\d\\d/\\d\\d)</td><td class=\"newsColM\">&nbsp;</td><td class=\"newsColCT\">(.+?)</td>";
            foreach (TickerItem tickerItem in tickerItems)
            {
                if (!tickerItem.FourTraderslink.Equals(""))
                {
                    string content = await DownloadUtil.ObtainEarningsDates(tickerItem);
                    var m = Regex.Match(content, pattern);
                    while (m.Success)
                    {
                        if (IsDateInFuture(m) && DateIsNotProjected(m) && IsNotSectorEvent(m))
                        {
                            System.Diagnostics.Debug.WriteLine(tickerItem.Ticker + " " + m.Groups[1].Value + " " + m.Groups[2].Value);
                            var item = new EarningsDateItem();
                            item.Ticker = tickerItem.Ticker;
                            item.ResultDate = DateUtil.ConvertToMySqlDate(DateUtil.ParseShortYearDate(m.Groups[1].Value));
                            item.Description = m.Groups[2].Value;
                            item.Country = Data.Yahoo.YahooDataManager.DataSetId.UK;
                            obtainedEarningsDateItems.Add(item);
                        }
                        m = m.NextMatch();
                    }
                }
            }
            return obtainedEarningsDateItems;
        }

        private static bool IsNotSectorEvent(Match m)
        {
            return !(m.Groups[2].Value.Contains("<b>") || m.Groups[2].Value.Contains("ex-dividend"));
        }

        private static bool DateIsNotProjected(Match m)
        {
            return !m.Value.Contains("Projected");
        }

        private static bool IsDateInFuture(Match m)
        {
            return DateUtil.ParseShortYearDate(m.Groups[1].Value) > DateTime.Now;
        }
    }
}
