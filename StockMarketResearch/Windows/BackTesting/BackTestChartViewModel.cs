﻿using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using StockMarketResearch.Data.Base;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System;
using System.Collections.Generic;
using System.IO;

namespace StockMarketResearch.Windows.BackTesting
{
    public class BackTestChartViewModel
    {
        YahooDataManager dataManager;

        IndicatorStrategyResultDao resultDao;

        public string Title { get; private set; }

        public static IList<DataPoint> Points { get; private set; }

        int iResult = 0;

        int timeInterval = 30 * 60; // 30 minutes

        public PlotModel plotModel { get; private set; }

        public BackTestChartViewModel()
        {
            dataManager = new YahooDataManager();

            plotModel = new PlotModel();
            var xAxis = new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                StringFormat = "dd/MM/yy HH:mm",
                IntervalLength = 75,
                MinorIntervalType = DateTimeIntervalType.Hours,
                IntervalType = DateTimeIntervalType.Days,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
            };

            var yPriceAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                Key = "priceAxis",
            };

            var yVolumeAxis = new LinearAxis
            {
                Position = AxisPosition.Right,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                Key = "volumeAxis",
            };

            plotModel.Axes.Add(xAxis);
            plotModel.Axes.Add(yPriceAxis);
            plotModel.Axes.Add(yVolumeAxis);

            addSeries();
            
        }

        private void addSeries()
        {
            plotModel.Series.Clear();

            //General

            string resultDaoJson = File.ReadAllText(dataManager.datasets[YahooDataManager.DataSetId.UK].analysisFolder + "//result.json");
            resultDao = JsonConvert.DeserializeObject<IndicatorStrategyResultDao>(resultDaoJson);
            plotModel.Title = resultDao.results[iResult].Ticker;
            YahooDataSet dataset;
            if (resultDao.results[iResult].Country == "US")
            {
                dataset = dataManager.datasets[YahooDataManager.DataSetId.US];
            }
            else
            {
                dataset = dataManager.datasets[YahooDataManager.DataSetId.UK];
            }
            string stockDaoJson = File.ReadAllText(dataset.pricesFolder + dataset.setFolder + "//" + resultDao.results[iResult].Ticker + ".json");
            StockDao stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);

            //Price series

            LineSeries priceSeries = new LineSeries();
            priceSeries.YAxisKey = "priceAxis";
            

            foreach (StockPriceSeriesItem item in stockDao.series)
            {

                if ((item.Timestamp > resultDao.results[iResult].OpenTimeStamp - timeInterval) && (item.Timestamp < resultDao.results[iResult].CloseTimeStamp + timeInterval))
                {
                    DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                    jTime = jTime.AddSeconds(item.Timestamp);
                    priceSeries.Points.Add(DateTimeAxis.CreateDataPoint(jTime, item.close));
                }
            }
            plotModel.Series.Add(priceSeries);

            //Open and Close series

            ScatterSeries scatterSeries = new ScatterSeries();
            DateTime openTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            openTime = openTime.AddSeconds(resultDao.results[iResult].OpenTimeStamp);
            scatterSeries.Points.Add(new ScatterPoint(DateTimeAxis.ToDouble(openTime), resultDao.results[iResult].Open));
            DateTime closeTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            closeTime = closeTime.AddSeconds(resultDao.results[iResult].CloseTimeStamp);

            scatterSeries.Points.Add(new ScatterPoint(DateTimeAxis.ToDouble(closeTime), resultDao.results[iResult].Close));

            plotModel.Series.Add(scatterSeries);

            //Volume series
            
            LineSeries volumeSeries = new LineSeries();
            volumeSeries.YAxisKey = "volumeAxis";

            foreach (StockPriceSeriesItem item in stockDao.series)
            {

                if ((item.Timestamp > resultDao.results[iResult].OpenTimeStamp - timeInterval) && (item.Timestamp < resultDao.results[iResult].CloseTimeStamp + timeInterval))
                {
                    DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                    jTime = jTime.AddSeconds(item.Timestamp);
                    volumeSeries.Points.Add(new DataPoint(DateTimeAxis.ToDouble(jTime), item.volume));
                }
            }
            plotModel.Series.Add(volumeSeries);

        }

        public void Next()
        {
            iResult++;
            addSeries();
        }

        public void Previous()
        {
            iResult--;
            addSeries();
        }
    }
}
