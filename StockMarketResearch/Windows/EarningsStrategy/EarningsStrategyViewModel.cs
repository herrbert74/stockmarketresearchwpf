﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using StockMarketResearch.Data.Yahoo;
using System;
using System.Linq;
using StockMarketResearch.Data.Storage;

namespace StockMarketResearch.Windows.EarningsStrategy
{
    public class EarningsStrategyViewModel
    {

        //public string Title { get; private set; }

        YahooDataManager dataManager = new YahooDataManager();

        public PlotModel plotModel { get; private set; }

        public EarningsStrategyResultDao Result { get; set; }

        int EarningsItem;

        public EarningsStrategyViewModel()
        {
            var xAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                LabelFormatter = _formatter,
                IntervalLength = 75,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                ExtraGridlineColor = OxyColor.FromArgb(255, 240, 22, 200),
                ExtraGridlineThickness = 2,
                Key = "xAxis",
            };

            var yPriceAxis = new LinearAxis
            {
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                ExtraGridlineColor = OxyColor.FromArgb(255, 40, 185, 184),
                ExtraGridlineThickness = 2,
                Key = "priceAxis",
            };

            plotModel = new PlotModel();
            plotModel.Axes.Add(xAxis);
            plotModel.Axes.Add(yPriceAxis);

        }

        private string _formatter(double item)
        {
            if (Result != null && item > 0 && item < Result.results[(int)EarningsItem].Series.KeyCount)
            {
                DateTime jTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                jTime = jTime.AddSeconds(Result.results[(int)EarningsItem].Series.GetAt((int)item).Timestamp);
                return jTime.ToShortDateString() + "\n" + jTime.ToShortTimeString();
            }
            else
            {
                return "";
            }
        }

        public void DisplayTicker(int earningsItem)
        {
            EarningsItem = earningsItem;
            //Reset
            plotModel.Series.Clear();
            plotModel.Axes[0].Reset();
            plotModel.Axes[1].Reset();
            bool isStartShown = false;
            bool isOpenShown = false;
            bool[] isCloseShown = new bool[Result.results[earningsItem].CloseTimeStamps.Count];
            for (int i = 0; i < isCloseShown.Count(); i++)
            {
                isCloseShown[i] = false;
            }

            double start = 0;
            double open = 0;
            double[] close = new double[Result.results[earningsItem].CloseTimeStamps.Count];

            LineSeries priceSeries = new LineSeries();

            foreach (var item in Result.results[earningsItem].Series.Values.Select((x, i) => new { x, i }))
            {
                priceSeries.Points.Add(new DataPoint(item.i, item.x.close));
                if (!isStartShown && Result.results[earningsItem].StartTimeStamp < item.x.Timestamp)
                {
                    isStartShown = true;
                    start = item.i - 1;

                }
                if (!isOpenShown && Result.results[earningsItem].OpenTimeStamp < item.x.Timestamp)
                {
                    isOpenShown = true;
                    open = item.i - 1;

                }
                for (int iterator = 0; iterator < isCloseShown.Count(); iterator++)
                {
                    if (!isCloseShown[iterator] && Result.results[earningsItem].CloseTimeStamps[iterator] < item.x.Timestamp)
                    {
                        isCloseShown[iterator] = true;
                        close[iterator] = item.i - 1;
                    }
                }
            }
            double[] TimeGridLines = new double[isCloseShown.Count() + 2];
            TimeGridLines[0] = start;
            TimeGridLines[1] = open;
            for (int iterator = 0; iterator < isCloseShown.Count(); iterator++)
            {
                TimeGridLines[iterator + 2] = close[iterator];
            }
            plotModel.Axes[0].ExtraGridlines = TimeGridLines;

            double[] PriceGridLines = new double[isCloseShown.Count() + 1];
            PriceGridLines[0] = Result.results[earningsItem].Open;
            for (int iterator = 0; iterator < isCloseShown.Count(); iterator++)
            {
                PriceGridLines[iterator + 1] = Result.results[earningsItem].ClosePrices[iterator];
            }
            plotModel.Axes[1].ExtraGridlines = PriceGridLines;

            //plotModel.Title = Result.results[earningsItem].Ticker;
            plotModel.Series.Add(priceSeries);

        }
    }
}
