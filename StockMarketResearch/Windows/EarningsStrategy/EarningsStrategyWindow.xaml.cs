﻿using System.Windows;
using System.Windows.Controls;
using StockMarketResearch.Data.Yahoo;
using System.Collections.Generic;
using StockMarketResearch.Strategies.EarningsStrategies;
using StockMarketResearch.Utils;
using StockMarketResearch.Data.Storage;
using StockMarketResearch.Data.Network;

namespace StockMarketResearch.Windows.EarningsStrategy
{
    public partial class EarningsStrategyWindow : Window
    {
        EarningsStrategyViewModel viewModel;
        
        int selectedTicker = 0;
        public YahooDataManager.DataSetId selectedDataSet = YahooDataManager.DataSetId.UK;
        EarningsStrategyResultDao Result;

        public EarningsStrategyWindow()
        {
            InitializeComponent();
            viewModel = (EarningsStrategyViewModel) this.DataContext;
            ExecuteStrategy();
            CanvasBorder.BorderThickness = new Thickness(1);
        }

        private async void ExecuteStrategy()
        {
            MultiCloseEarningsStrategy simpleEarningsStrategy = new MultiCloseEarningsStrategy();
            Result = await simpleEarningsStrategy.CalculateResult();
            AverageResult.Text = Result.averageResult.ToString();
            Wins.Text = Result.pos.ToString();
            AverageWin.Text = Result.averageWin.ToString();
            Loss.Text = Result.neg.ToString();
            AverageLoss.Text = Result.averageLoss.ToString();
            TextVariance.Text = Result.variance.ToString();
            TextSharpe.Text = Result.sharpeRatio.ToString();
            FillListBox(Result);
            viewModel.Result = Result;
            DisplayTicker(0);
        }

        private void FillComboBox(List<EarningsDateItem> stockResponse)
        {
            foreach (EarningsDateItem resultdateObject in stockResponse) {
                cmb.Items.Add(resultdateObject.Ticker);
            }
        }

        private void FillListBox(EarningsStrategyResultDao resultDao)
        {
            List<ListBoxItem> items = new List<ListBoxItem>();

            listbox.ItemsSource = items;
            foreach (EarningsStrategyResultDao.EarningsResult resultDaoItem in resultDao.results)
            {
                items.Add(new ListBoxItem() { Ticker = resultDaoItem.Ticker, Result = string.Format("{0:P2}.", (resultDaoItem.TradeResult)) });
            }
            listbox.ItemsSource = items;
        }

        public class ListBoxItem
        {
            public string Ticker { get; set; }
            public string Result { get; set; }
        }

        private void Button_Previous_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker > 0)
            {
                DisplayTicker(--selectedTicker);
            }
        }

        private void Button_Next_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker < Result.results.Count - 1)
            {
                DisplayTicker(++selectedTicker);
            }
        }

        private void Button_Previous_10_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker > 9)
            {
                selectedTicker -= 10;
                DisplayTicker(selectedTicker);
            }
        }

        private void Button_Next_10_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTicker < Result.results.Count - 10)
            {
                selectedTicker += 10;
                DisplayTicker(selectedTicker);
            }
        }

        private void ComboBox_Selected(object sender, RoutedEventArgs e)
        {
            ComboBoxItem typeItem = (ComboBoxItem)cmb.SelectedItem;
            if (typeItem.Content != null)
            {
                string selectedText = typeItem.Content.ToString();
                if (selectedText.Equals("UK"))
                {                    
                    selectedDataSet = YahooDataManager.DataSetId.UK;
                    selectedTicker = 0;
                }
                else
                {
                    selectedDataSet = YahooDataManager.DataSetId.US;
                    selectedTicker = 0;
                }
                DisplayTicker(selectedTicker);
            }
        }
        
        private void DisplayTicker(int selectedTicker)
        {
            EarningsStrategyResultDao.EarningsResult TickerResult = Result.results[selectedTicker];

            listbox.SelectedIndex = selectedTicker;
            listbox.ScrollIntoView(listbox.SelectedItem);
            viewModel.DisplayTicker(selectedTicker);

            TextSelectedTicker.Text = (selectedTicker + 1).ToString() + ". " + TickerResult.Ticker;
            TextCompany.Text = TickerResult.Company;
            TextOpenTime.Text = DateUtil.FormatUnixTimeStamp(TickerResult.OpenTimeStamp);
            TextOpenAmount.Text = TickerResult.Open.ToString();
            TextCloseTime.Text = DateUtil.FormatUnixTimeStamp(TickerResult.CloseTimeStamps[TickerResult.CloseTimeStamps.Count - 1]);
            TextCloseAmount.Text = TickerResult.ClosePrices[TickerResult.ClosePrices.Count - 1].ToString();
            TextWinPercentage.Text = string.Format("{0:P2}.", TickerResult.TradeResult);

            plot.InvalidatePlot(true);
        }

        private void Button_Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow w = new MainWindow();
            w.Show();
            this.Close();
        }

        private void listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedTicker = listbox.SelectedIndex;
            DisplayTicker(selectedTicker);
        }
    }
}
