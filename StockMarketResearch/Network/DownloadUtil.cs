﻿using Newtonsoft.Json;
using Refit;
using StockMarketResearch.Data.Base;
using StockMarketResearch.Data.Network;
using StockMarketResearch.Data.Yahoo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public static class DownloadUtil
    {
        public static async Task<YahooStockResponse> downloadGoogleIntraDayData(YahooDataManager.DataSetId id, string ticker)
        {
            int INTERVAL = 300;
            var exchange = id==YahooDataManager.DataSetId.UK ? "LON" : (id == YahooDataManager.DataSetId.US ? (YahooDataManager.nasdaqUniverse.Contains(ticker) ? "NASDAQ" : "NYSE") : (ticker.Equals("^gspc") ? "INDEXSP" : "INDEXFTSE"));
            var ticker_mod = ticker.Replace(".L", "");
            convertTickerForGoogleIntraDay(ref ticker_mod);
            var googleIntraDayDataService = RestService.For<IGoogleIntraDayService>("http://www.google.com");
            var response = await googleIntraDayDataService.GetData(ticker_mod, exchange, INTERVAL, 50);
            
            var yahooStockResponse = new YahooStockResponse();
            var series = new List<StockPriceSeriesItem>();
            var meta = new Meta();
            meta.ticker = ticker;
            yahooStockResponse.meta = meta;
            System.Diagnostics.Debug.WriteLine("ticker: " + ticker + " " + ticker_mod);
            int line_number = 0;
            long startTimeStamp = 0;
            var lines = response.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var s in lines)
            {
                line_number++;

                if (line_number > 7)
                {
                    var words = s.Split(',');
                    var item = new StockPriceSeriesItem();
                    if (startTimeStamp == 0 || words[0].StartsWith("a", StringComparison.Ordinal))
                    {
                        try
                        {
                            startTimeStamp = long.Parse(words[0].Substring(1));
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        item.Timestamp = startTimeStamp;
                    }
                    else if (words[0].StartsWith("a", StringComparison.Ordinal))
                    {
                        try
                        {
                            item.Timestamp = long.Parse(words[0].Substring(1));
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        item.Timestamp = startTimeStamp;
                    }
                    else
                    {
                        try
                        {
                            item.Timestamp = startTimeStamp + long.Parse(words[0]) * INTERVAL;
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                    item.open = Double.Parse(words[1]);
                    item.high = Double.Parse(words[2]);
                    item.low = Double.Parse(words[3]);
                    item.close = Double.Parse(words[4]);
                    item.volume = int.Parse(words[5]);
                    series.Add(item);
                }
            }

            yahooStockResponse.series = series;
            return yahooStockResponse;
        }

        private static void convertTickerForGoogleIntraDay(ref string ticker)
        {
            if (ticker.Equals("^ftse"))
            {
                ticker = "UKX";
            }
            else if (ticker.Equals("^ftmc"))
            {
                ticker = "MCX";
            }else if (ticker.Equals("^gspc"))
            {
                ticker = "INX";
            } else if (ticker.Equals("BT-A"))
            {
                ticker = "BT.A";
            } else if (ticker.Equals("BRK-B"))
            {
                ticker = "BRK.B";
            }
        }

        public static async Task<YahooStockResponse> downloadYahooFinanceData(YahooDataManager.DataSetId id, string ticker)
        {
            var stockService = RestService.For<IYahooStockService>("http://chartapi.finance.yahoo.com");
            var response = await stockService.GetData(ticker, "25");
            var newBody = Regex.Replace(response, "finance_charts_json_callback\\(", "");
            newBody = Regex.Replace(newBody, "\\)$", "");
            return JsonConvert.DeserializeObject<YahooStockResponse>(newBody);
        }

        public static async Task<string> downloadYahooFinanceDaily(int iTickerRanking, List<TickerItem> dataset)
        {
            var yahooStockService = RestService.For<IYahooDailyStockService>("http://download.finance.yahoo.com");

            return await yahooStockService.GetYahooDataCsv(dataset[iTickerRanking].Ticker, "sj2"); //Ticker and Shares outstanding
        }

        public static async Task<List<TickerItem>> DownloadTickerList()
        {
            var stockService = RestService.For<IGetTickersService>("http://herrbert74.biz.ht");
            var response = await stockService.GetTickers();
            return JsonConvert.DeserializeObject<List<TickerItem>>(response);
        }

        public static async Task<string> UpdateSharesOutstanding(string ticker, string shares)
        {
            var data = new Dictionary<string, object> {
               {"ticker", ticker},
               {"shares", shares}
            };
            var service = RestService.For<IUpdateSharesOutstandingService>("http://herrbert74.biz.ht");
            return await service.SetSharesOutstanding(data);
        }

        public static async Task<List<EarningsDateItem>> DownloadEarningsDates()
        {
            var stockService = RestService.For<IEarningsDatesService>("http://herrbert74.biz.ht");
            var response = await stockService.GetResultDates();
            return JsonConvert.DeserializeObject<List<EarningsDateItem>>(response);
        }

        public static async Task<string> ObtainEarningsDates(TickerItem tickerItem)
        {
            var stockService = RestService.For<IObtainEarningDatesService>("http://www.4-traders.com");
            return await stockService.GetEarningDates(tickerItem.FourTraderslink);
        }

        public static async Task<string> UploadEarningsDate(string date, string ticker, string country, string description)
        {
            var data = new Dictionary<string, object> {
                {"date", date},
               {"ticker", ticker},
               {"country", country},
               {"description", description}
            };
            var service = RestService.For<IUploadEarningsDateService>("http://herrbert74.biz.ht");
            return await service.UpdateEarningsDate(data);
        }

        public static async Task<GoogleStockPriceSeriesItem[]> DownloadGoogleFinanceDaily(string ticker)
        {
            convertTicker(ref ticker);
            //var stockService = RestService.For<IQuandlStockService>("https://www.quandl.com");
            var stockService = RestService.For<IGoogleDailyStockService>("https://www.google.com");
            //QuandlStockResponse stockResponse = JsonConvert.DeserializeObject<QuandlStockResponse>(response);
            var response = await stockService.GetData(ticker);
            return getDailyPrices(response);
        }

        private static void convertTicker(ref string ticker)
        {
            if (ticker.Equals("BT-A.L"))
            {
                ticker = "bt.l";
            }
            else if (ticker.Equals("IMT.L"))
            {
                ticker = "imb.l";
            }
            else if (ticker.Equals("CAPC.L"))
            {
                ticker = "capc";
            }
        }

        private static GoogleStockPriceSeriesItem[] getDailyPrices(string response)
        {
            var lines = response.Split('\n');
            GoogleStockPriceSeriesItem[] items = new GoogleStockPriceSeriesItem[lines.Length - 2];
            for (int i = lines.Length - 2; i > 0; i--)
            {

                //Process row
                var fields = lines[i].Split(',');
                var item = new GoogleStockPriceSeriesItem();
                item.Date = fields[0];
                //Always good?!
                item.close = Double.Parse(fields[4]);
                if (fields[1].Equals("-"))
                {
                    //Take previous close, which is in the next line (i+1), but items is ofset by one in other direction (i-1), so we need i
                    item.open = items[i].close;
                }
                else
                {
                    item.open = Double.Parse(fields[1]);
                }
                if (fields[2].Equals("-"))
                {
                    //Data is not displayed, because it is either equals to the opening or the closing price
                    item.high = Math.Max(item.open, item.close);
                }
                else
                {
                    item.high = Double.Parse(fields[2]);
                }
                if (fields[3].Equals("-"))
                {
                    //Data is not displayed, because it is either equals to the opening or the closing price
                    item.low = Math.Min(item.open, item.close);
                }
                else
                {
                    item.low = Double.Parse(fields[3]);
                }

                item.volume = int.Parse(fields[5]);
                items[i - 1] = item;
            }

            return items;
        }
    }
}
