﻿using Refit;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IYahooDailyStockService
    {
        [Get("/d/quotes.csv?s={ticker}&f={tags}&e=.csv")]
        Task<string> GetYahooDataCsv(string ticker, string tags);
    }
}
