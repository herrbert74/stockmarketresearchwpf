﻿using Refit;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IGoogleIntraDayService
    {
        [Get("/finance/getprices?q={ticker}&x={exchange}&i={interval}&p={days}d&f=d,o,h,l,c,v")]
        Task <string> GetData(string ticker, string exchange, int interval, int days);
    }
}
