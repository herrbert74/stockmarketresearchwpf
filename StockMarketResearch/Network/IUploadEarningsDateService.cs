﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IUploadEarningsDateService
    {
        [Put("/companyreports/api.php?rquest=updateearningsdate")]
        Task<string> UpdateEarningsDate([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, object> data);
    }
    
}
