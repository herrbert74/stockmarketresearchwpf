﻿using Refit;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IGoogleDailyStockService
    {
        //[Get("/api/v3/datasets/GOOG/LON_{ticker}.json?api_key=J3Wkbu3tgRqzW2d3WQjR")]
        [Get("/finance/historical?q={ticker}&output=csv")]
        Task <string> GetData(string ticker);
    }
}
