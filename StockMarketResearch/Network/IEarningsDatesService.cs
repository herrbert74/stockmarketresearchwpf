﻿using Refit;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IEarningsDatesService
    {
        [Get("/companyreports/api.php?rquest=getresults")]
        Task<string> GetResultDates();
    }
}
