﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IGetTickersService
    {
        [Get("/ranking/api.php?rquest=gettickers")]
        Task<string> GetTickers();
    }

    public interface IUpdateSharesOutstandingService
    {
        [Put("/ranking/api.php?rquest=updateshares")]
        Task<string> SetSharesOutstanding([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, object> data);
        //Task<string> SetSharesOutstanding([Body] string ticker, string shares);
    }
}
