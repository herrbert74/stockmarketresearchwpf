﻿using Refit;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IIndexChangesService
    {
        [Get("/indexchanges/api.php?rquest=getindexchanges")]
        Task <string> GetIndexChanges();
    }
}
