﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IObtainEarningDatesService
    {
        [Get("/{link}/calendar/")]
        Task<string> GetEarningDates(string link);
    }
    
}
