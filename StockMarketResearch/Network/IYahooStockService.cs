﻿using Refit;
using System.Threading.Tasks;

namespace StockMarketResearch.Network
{
    public interface IYahooStockService
    {
        [Get("/instrument/1.0/{ticker}/chartdata;type=quote;range={days}d/json")]
        Task<string> GetData(string ticker, string days);
    }
}
