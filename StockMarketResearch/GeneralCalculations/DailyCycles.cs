﻿using Newtonsoft.Json;
using StockMarketResearch.Data.Base;
using StockMarketResearch.Data.Yahoo;
using StockMarketResearch.Data.Storage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StockMarketResearch.GeneralCalculations
{
    class DailyCycles
    {
        static HashSet<double>[] ftse100values = new HashSet<double>[60 * 60 * 24 / 5];
        static HashSet<double>[] ftse250values = new HashSet<double>[60 * 60 * 24 / 5];
        static HashSet<double>[] sp500values = new HashSet<double>[60 * 60 * 24 / 5];

        YahooDataManager dataManager = new YahooDataManager();

        public void calculate()
        {
            foreach (KeyValuePair<YahooDataManager.DataSetId, YahooDataSet> dataset in dataManager.datasets)
            {
                if (dataset.Key != YahooDataManager.DataSetId.INDEXES)
                {
                    foreach (string ticker in dataset.Value.universe.tickers)
                    {
                        addPricesForTicker(dataset, ticker);
                    }
                }
            }
        }

        public static void addPricesForTicker(KeyValuePair<YahooDataManager.DataSetId, YahooDataSet> dataset, string ticker)
        {
            HashSet<double>[] actual;
            switch (dataset.Key){
                case YahooDataManager.DataSetId.UK:
                    actual = ftse100values;
                    break;
                case YahooDataManager.DataSetId.US:
                    actual = sp500values;
                    break;
                default:
                    actual = ftse100values;
                    break;
            }

            string stockDaoJson = File.ReadAllText(dataset.Value.cyclesFolder + ticker + ".json");

            StockDao stockDao = JsonConvert.DeserializeObject<StockDao>(stockDaoJson);
            long previousDay = 0, currentDay = 0;
            double previousdayClose = 0, latestClose = 0;
            foreach (StockPriceSeriesItem seriesItem in stockDao.series)
            {
                if (previousDay == 0)
                {
                    previousDay = roundDown(seriesItem.Timestamp, 60 * 60 * 24) - 60 * 60 * 24;
                    currentDay = roundDown(seriesItem.Timestamp, 60 * 60 * 24);
                    previousdayClose = seriesItem.close;
                }
                DateTime itemTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                itemTime = itemTime.AddSeconds(seriesItem.Timestamp);
                long itemDay = roundDown(seriesItem.Timestamp, 60 * 60 * 24);
                if (currentDay != itemDay)
                {
                    previousDay = currentDay;
                    currentDay = itemDay;
                    previousdayClose = latestClose;
                }
                latestClose = seriesItem.close;
                long timeOfDay = seriesItem.Timestamp - currentDay;
                double value = seriesItem.close / previousdayClose;
                if(actual[(int)(timeOfDay / (60 * 5))] == null)
                {
                    actual[(int)(timeOfDay / (60 * 5))] = new HashSet<double>();
                }
                actual[(int)(timeOfDay / (60 * 5)) ].Add(value);
            }

            ArrayList vals = new ArrayList();
            for(int i = 0; i < actual.Count(); i++)
            {
                if(actual[i] != null)
                {
                    double result = 0;
                    foreach(double v in actual[i])
                    {
                        result += v;
                    }
                    result = result / actual[i].Count;
                    vals.Add(result);
                }
                
            }


            string json = JsonConvert.SerializeObject(vals, Formatting.Indented);
            if (!File.Exists(dataset.Value.cyclesFolder + dataset.Value.setFolder))
            {
                Directory.CreateDirectory(dataset.Value.cyclesFolder + dataset.Value.setFolder);
            }
            File.WriteAllText(dataset.Value.cyclesFolder + dataset.Value.setFolder + ".json", json);
        }

        public static long roundDown(long toRound, long multipleOf)
        {
            return toRound - toRound % multipleOf;
        }
    }
}
