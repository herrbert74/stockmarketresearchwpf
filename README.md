# README #

Stock Market Research app.

### Features ###

* Download and store intra day data from Yahoo Finance.
* Calculate indicators.
* Calculate results for strategies (currently earnings strategy). Download earnings dates from MySQL database.
* Ranking of FTSE stocks from Yahoo Finance statistics and proprietary list of tickers and index constituents.

### List of Refit network calls ###

* Yahoo Finance
    * Intra day data
    * Statistics (shares outstanding) for ranking
* Google Finance daily data
* Proprietary
    * Ticker list (needed for Ranking)
    * Index constituent changes (needed for Ranking)
    * Update shares outstanding for ticker list (needed for Ranking)
    * Earnings dates